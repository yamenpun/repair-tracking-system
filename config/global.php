<?php
return [
    /*
    |--------------------------------------------------------------------------
    | ADMIN ROLE
    |--------------------------------------------------------------------------
    |
    */
    'admin-role'  => [
        'admin'     => 'Admin',
        'counselor' => 'Technician',
    ],

    /*
    |--------------------------------------------------------------------------
    | ITEM STATUS - CASE_HISTORY TABLE
    |--------------------------------------------------------------------------
    |
    */
    'item_status' => [
        'received'       => 'Received',
        'inspected'      => 'Inspected',
        'wip'            => 'Work In Progress',
        'repaired'       => 'Repaired',
        'completed'      => 'Completed',
        'contact_office' => 'Contact Office',
        'delivered'      => 'Delivered',
        'withdrawal'     => 'Withdrawal',
    ],

    /*
   |--------------------------------------------------------------------------
   | Paths: We will list all the project public paths here
   |--------------------------------------------------------------------------
   |
   */
    'path'        => [
        'frontend' => [
            'image' => public_path() . "\\assets\\images\\"
        ],
        'backend'  => [

        ]
    ],
    'url'         => [
        'frontend' => [
            'image' => 'assets/images/'
        ],
        'backend'  => [

        ]
    ],

];




