<?php
namespace App\HelperClass;

use Illuminate\Http\Request;
use DB;

class AppHelper {

    /**
     * Generates html according to message_type and stores in
     * session flash storage
     *
     * @param $message_type bootstrap alert message type
     * @param $message html message
     */
    public function flash($message_type, $message)
    {
        $message_type = $this->checkBootstrapAlertClass($message_type);

        $message = "<div class=\"alert alert-" . $message_type . "\">
                        <button data-dismiss=\"alert\" class=\"close\" type=\"button\">
                            <i class=\"icon-remove\"></i>
                        </button>
                        " . $message . "
                        <br>
					</div>";

        request()->session()->flash('message', $message);
    }

    protected function checkBootstrapAlertClass($message_type)
    {
        $classes = ['info', 'success', 'warning', 'danger'];
        if (!in_array($message_type, $classes))
        {
            return 'info';
        }

        return $message_type;
    }

    /**
     * @param $errors
     * @param $field_name
     * @return string
     */
    public function getValidationErrorMsg($errors, $field_name)
    {
        if ($errors->has($field_name))
        {
            return '<strong class="help-block validation-error">' . $errors->first('caption_one') . "</strong>";
        }

        return '';
    }

    /**
     * @param $item_status
     * @return string
     */
    public function getItemStatusDetail($item_status)
    {
        if ($item_status == 'delivered')
        {
            return "Item has been delivered.";
        } elseif ($item_status == 'contact_office')
        {
            return "Please contact our office.";
        }elseif ($item_status == 'inspected')
        {
            return "Item is being inspected.";
        }elseif ($item_status == 'received')
        {
            return "Item has been received.";
        }elseif ($item_status == 'repaired')
        {
            return "Item has been repaired.";
        }elseif ($item_status == 'wip')
        {
            return "Work is under progress.";
        }elseif ($item_status == 'withdrawal')
        {
            return "Item has been withdrawn.";
        }elseif ($item_status == 'completed')
        {
            return "Item is ready.";
        }
    }
    
    /**
     * @return string
     */
    public function getCaseCode()
    {
        $a = 'LNCS';
        $y = date('Y');
        
        $data['case'] = DB::select(DB::raw(" SELECT case_code FROM cases ORDER BY id DESC LIMIT 1 "));
        if ($data['case'] == null) // check if there is old case code or not
        {
            $i = sprintf('%04u', 1);
            $case_code = $a . $y . $i;

            return $case_code;

        } else // if there is old value then
        {
            $old_case_code = $data['case']['0']->case_code;
            $case_code_array = str_split($old_case_code, 4);
            if ($case_code_array[1] == $y) // check if the current year is equal to year of latest case code
            {
                $old_case_code_no = array_pop($case_code_array) + 1;
                $new_case_code_no = sprintf('%04u', $old_case_code_no);
                $case_code = $a . $y . $new_case_code_no;

                return $case_code;
            } else
            {
                $i = sprintf('%04u', 1);
                $case_code = $a . $y . $i;

                return $case_code;
            }
        }
    }

    public function getUsername($fullname = 'Binaya Pun')
    {
        $array = explode(' ', $fullname);

        $sample_username = strtolower($array['0']);

        $username_list = User::select('username')->get();

        foreach($username_list as $old_username)
        {
            if($old_username->username = $sample_username)
            {
                return $username = $sample_username.'1'.'123';
            }else{
                return $username = $sample_username.'123';
            }
        }
    }

}