<?php

namespace App\Models;

class Payments extends BaseModel
{
    protected $table = 'payments';

    protected $fillable = ['id', 'customer_id', 'amount_paid', 'remarks'];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
