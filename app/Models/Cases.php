<?php

namespace App\Models;

class Cases extends BaseModel
{
    protected $table = 'cases';

    protected $fillable = [
        'id', 'case_code', 'case_date', 'customer_id', 'item_id','case_name',
        'problem_description', 'item_brand', 'item_model', 'item_specs',
        'minimum_charge', 'vendor_charge', 'total_charge', 'remarks', 'handled_by'
    ];


    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Items');
    }

    public function cases_history()
    {
        return $this->hasMany('App\Models\CasesHistory');
    }

    public function case_comment()
    {
        return $this->hasMany('App\Models\CaseComment');
    }
    
}
