<?php

namespace App\Models;

class CasesHistory extends BaseModel
{
    protected $table = 'cases_history';

    protected $fillable = ['id', 'case_history_date', 'case_id', 'item_status', 'description', 'image_one', 'image_two', 'remarks', 'etr', 'charge'];

    public function cases()
    {
        return $this->belongsTo('App\Models\Cases');
    }

    public function case_history_comment()
    {
        return $this->hasMany('App\Models\CaseHistoryComment');
    }
}
