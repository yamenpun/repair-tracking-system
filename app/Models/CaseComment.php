<?php

namespace App\Models;

class CaseComment extends BaseModel
{
    protected $table = 'case_comment';

    protected $fillable = ['id', 'comment_id', 'case_id', 'case_comment_desc', 'status'];

    public function cases()
    {
        return $this->belongsTo('App\Models\Cases');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }


}
