<?php

namespace App\Models;


class Items extends BaseModel
{
    protected $table = 'items';

    protected $fillable = ['id', 'item_type', 'item_description'];

    public function cases()
    {
        return $this->hasMany('App\Models\Cases');
    }
}
