<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'fullname', 'username', 'office_name', 'email', 'role', 'password', 'mobile_number', 'home_number', 'address', 'status', 'remarks', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cases()
    {
        return $this->hasMany('App\Models\Cases');
    }

    public function case_comment()
    {
        return $this->hasMany('App\Models\CaseComment');
    }

    public function case_history_comment()
    {
        return $this->hasMany('App\Models\CaseHistoryComment');
    }

}
