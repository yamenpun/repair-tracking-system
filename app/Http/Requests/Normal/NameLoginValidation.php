<?php

namespace App\Http\Requests\Normal;

use App\Http\Requests\Request;

class NameLoginValidation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'fullname.required' =>  'Please enter you Full Name.',
        ];
    }
}
