<?php

namespace App\Http\Requests\Normal;

use App\Http\Requests\Request;

class CodeLoginValidation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'case_code'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'case_code.required' =>  'Please enter your enter case code.',
        ];
    }
}
