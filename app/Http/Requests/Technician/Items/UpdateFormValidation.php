<?php

namespace App\Http\Requests\Technician\Items;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'item_name'  => 'required',
            'item_order' => 'required|numeric',
        ];
    }
}
