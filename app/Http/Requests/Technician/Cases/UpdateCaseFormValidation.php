<?php

namespace App\Http\Requests\Technician\Cases;

use App\Http\Requests\Request;

class UpdateCaseFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'case_name'      => 'required',
            'item_brand'     => 'required',
            'item_model'     => 'required',
            'item_specs'     => 'required',
            'minimum_charge' => 'required|numeric',
        ];
    }
}
