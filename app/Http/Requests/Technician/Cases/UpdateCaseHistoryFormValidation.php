<?php

namespace App\Http\Requests\Technician\Cases;

use App\Http\Requests\Request;

class UpdateCaseHistoryFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'etr'          => 'required|numeric',
            'description'  => 'required',
            'total_charge' => 'required|numeric'
        ];
    }
}
