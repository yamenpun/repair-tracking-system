<?php

namespace App\Http\Requests\Technician\Company;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'fullname'      => 'required',
            'office_name'   => 'required',
            'username'      => 'required|min:3|unique:users,username,' . $this->request->get('id') . ',id',
            'email'         => 'email|unique:users,email,' . $this->request->get('id') . ',id',
            'password'      => 'confirmed',
            'mobile_number' => 'required'
        ];
    }
}
