<?php

namespace App\Http\Requests\Admin\Cases;

use App\Http\Requests\Request;

class AddCaseFormValidation extends Request {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'case_name'           => 'required',
            'problem_description' => 'required',
            'item_brand'          => 'required',
            'minimum_charge'      => 'numeric',
        ];
    }

}
