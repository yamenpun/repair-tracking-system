<?php

namespace App\Http\Requests\Admin\Cases;

use App\Http\Requests\Request;

class AddCaseHistoryFormValidation extends Request {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'  => 'required',
            'total_charge' => 'numeric'
        ];
    }

}
