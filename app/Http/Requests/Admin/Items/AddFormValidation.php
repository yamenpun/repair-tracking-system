<?php

namespace App\Http\Requests\Admin\Items;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_type'      => 'required',
        ];
    }
    
}
