<?php

namespace App\Http\Requests\Admin\CaseHistoryComment;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'case_history_comment_desc'      => 'required',
        ];
    }
    
}
