<?php

namespace App\Http\Requests\Admin\Company;

use App\Http\Requests\Request;

class AddFormValidation extends Request {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'      => 'required',
            'office_name'   => 'required',
            'username'      => 'required|min:3|unique:users,username',
            'email'         => 'email|unique:users,email',
            'password'      => 'required',
            'mobile_number' => 'required'
        ];
    }

}
