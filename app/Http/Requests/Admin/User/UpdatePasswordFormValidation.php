<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Request;

class UpdatePasswordFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'password'      => 'required|confirmed',
        ];
    }
}
