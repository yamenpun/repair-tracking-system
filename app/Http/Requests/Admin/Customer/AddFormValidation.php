<?php

namespace App\Http\Requests\Admin\Customer;

use App\Http\Requests\Request;

class AddFormValidation extends Request {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'      => 'required',
            'username'      => 'required|min:3|unique:users,username',
            'email'         => 'email|unique:users,email',
            'password'      => 'required',
            'mobile_number' => 'required'
        ];
    }

}
