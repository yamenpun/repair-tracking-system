<?php

namespace App\Http\Requests\Customer\Account;

use App\Http\Requests\Request;

class AddCaseCommentValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'case_comment_desc'      => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'case_comment_desc.required' => 'Please do not leave message field blank.',
        ];
    }
}
