<?php

namespace App\Http\Requests\Company\Cases;

use App\Http\Requests\Request;

class UpdateChargeFormValidation extends Request {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor_charge' => 'required|numeric'
        ];
    }

}
