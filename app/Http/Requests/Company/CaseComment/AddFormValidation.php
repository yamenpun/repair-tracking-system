<?php

namespace App\Http\Requests\Company\CaseComment;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'case_comment_desc'      => 'required',
        ];
    }
    
}
