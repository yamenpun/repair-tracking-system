<?php

use App\User;
use Illuminate\Support\Facades\Input;

Route::get('admin/login', ['as' => 'admin.login', 'uses' => 'HomeController@index']);

Route::get('/ajax-fullname', function ()
{
    $fullname = Input::get('fullname');

    $array = explode(' ', $fullname);

    $uname = strtolower($array['0']);

    $new_username = $uname;

    $users = User::select('username')->where('username', $new_username)->get();

    if ($users->count() > 0)
    {
        $username = $new_username . '123';

        return Response::json($username);
    } else
    {
        $username = $new_username;

        return Response::json($username);
    }
});

Route::auth();

// For Normal Login
Route::get('/',                     ['as' => 'normal.code-login-form',      'uses' => 'Normal\CustomerController@showCaseCodeLoginForm']);
Route::post('normal/login',         ['as' => 'normal.code-login',           'uses' => 'Normal\CustomerController@caseCodeLogin']);
Route::get('normal/name/login',     ['as' => 'normal.name-login-form',      'uses' => 'Normal\CustomerController@showNameLoginForm']);
Route::post('normal/name/login',    ['as' => 'normal.name-login',           'uses' => 'Normal\CustomerController@nameLogin']);
Route::get('normal/dashboard',      ['as' => 'normal.dashboard',            'uses' => 'Normal\CustomerController@dashboard']);
Route::get('normal/logout',         ['as' => 'normal.logout',               'uses' => 'Normal\CustomerController@logout']);

// For Case Comment and Case Comment History
Route::get('normal/case/comment/{id}',                  ['as' => 'normal.case.comment',                 'uses' => 'Normal\CaseCommentController@showCaseComment']);
Route::post('normal/case/comment/store/{id}',           ['as' => 'normal.case.comment.store',           'uses' => 'Normal\CaseCommentController@storeCaseComment']);
Route::get('normal/case/history/comment/{id}',          ['as' => 'normal.case.history.comment',         'uses' => 'Normal\CaseHistoryCommentController@showCaseHistoryComment']);
Route::post('normal/case/history/comment/store/{id}',   ['as' => 'normal.case.history.comment.store',   'uses' => 'Normal\CaseHistoryCommentController@storeCaseHistoryComment']);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function ()
{
    // For Admin Dashboard
    Route::get('dashboard',     ['as' => 'admin.dashboard',     'uses' => 'Admin\DashboardController@index']);

    // For Admin User
    Route::get('user',                          ['as' => 'admin.user',                  'uses' => 'Admin\UserController@index']);
    Route::get('user/add',                      ['as' => 'admin.user.add',              'uses' => 'Admin\UserController@create']);
    Route::post('user/store',                   ['as' => 'admin.user.store',            'uses' => 'Admin\UserController@store']);
    Route::get('user/edit/{id}',                ['as' => 'admin.user.edit',             'uses' => 'Admin\UserController@edit']);
    Route::post('user/update/{id}',             ['as' => 'admin.user.update',           'uses' => 'Admin\UserController@update']);
    Route::get('user/delete/{id}',              ['as' => 'admin.user.delete',           'uses' => 'Admin\UserController@destroy']);
    Route::get('user/profile',                  ['as' => 'admin.user.profile',          'uses' => 'Admin\UserController@profile']);
    Route::get('user/edit/password/{id}',       ['as' => 'admin.user.edit.password',    'uses' => 'Admin\UserController@editPassword']);
    Route::post('user/update/password/{id}',    ['as' => 'admin.user.update.password',  'uses' => 'Admin\UserController@updatePassword']);

    // For Admin Customer
    Route::get('customer',                      ['as' => 'admin.customer',                  'uses' => 'Admin\CustomerController@index']);
    Route::get('customer/add',                  ['as' => 'admin.customer.add',              'uses' => 'Admin\CustomerController@create']);
    Route::post('customer/store',               ['as' => 'admin.customer.store',            'uses' => 'Admin\CustomerController@store']);
    Route::get('customer/view/{id}',            ['as' => 'admin.customer.view',             'uses' => 'Admin\CustomerController@view']);
    Route::get('customer/edit/{id}',            ['as' => 'admin.customer.edit',             'uses' => 'Admin\CustomerController@edit']);
    Route::post('customer/update/{id}',         ['as' => 'admin.customer.update',           'uses' => 'Admin\CustomerController@update']);
    Route::get('customer/delete/{id}',          ['as' => 'admin.customer.delete',           'uses' => 'Admin\CustomerController@destroy']);
    Route::get('customer/profile',              ['as' => 'admin.customer.profile',          'uses' => 'Admin\CustomerController@profile']);
    Route::get('customer/print',                ['as' => 'admin.customer.print',            'uses' => 'Admin\CustomerController@exportAsPrint']);
    Route::get('customer/pdf',                  ['as' => 'admin.customer.pdf',              'uses' => 'Admin\CustomerController@exportAsPdf']);
    Route::get('customer/excel',                ['as' => 'admin.customer.excel',            'uses' => 'Admin\CustomerController@exportAsExcel']);
    Route::get('customer/print/details/{id}',   ['as' => 'admin.customer.print.details',    'uses' => 'Admin\CustomerController@printCustomerDetail']);

    // For Admin Items
    Route::get('items/list',            ['as' => 'admin.items.list',        'uses' => 'Admin\ItemsController@index']);
    Route::get('items/add',             ['as' => 'admin.items.add',         'uses' => 'Admin\ItemsController@create']);
    Route::post('items/store',          ['as' => 'admin.items.store',       'uses' => 'Admin\ItemsController@store']);
    Route::get('items/edit/{id}',       ['as' => 'admin.items.edit',        'uses' => 'Admin\ItemsController@edit']);
    Route::post('items/update/{id}',    ['as' => 'admin.items.update',      'uses' => 'Admin\ItemsController@update']);
    Route::get('items/delete/{id}',     ['as' => 'admin.items.delete',      'uses' => 'Admin\ItemsController@destroy']);
    Route::get('items/print',           ['as' => 'admin.items.print',       'uses' => 'Admin\ItemsController@exportAsPrint']);
    Route::get('items/pdf',             ['as' => 'admin.items.pdf',         'uses' => 'Admin\ItemsController@exportAsPdf']);
    Route::get('items/excel',           ['as' => 'admin.items.excel',       'uses' => 'Admin\ItemsController@exportAsExcel']);

    // For Admin Cases
    Route::get('cases/list',                ['as' => 'admin.cases.list',            'uses' => 'Admin\CasesController@index']);
    Route::get('cases/add',                 ['as' => 'admin.cases.add',             'uses' => 'Admin\CasesController@create']);
    Route::post('cases/store',              ['as' => 'admin.cases.store',           'uses' => 'Admin\CasesController@store']);
    Route::get('cases/view/{id}',           ['as' => 'admin.cases.view',            'uses' => 'Admin\CasesController@view']);
    Route::get('cases/edit/{id}',           ['as' => 'admin.cases.edit',            'uses' => 'Admin\CasesController@edit']);
    Route::post('cases/update/{id}',        ['as' => 'admin.cases.update',          'uses' => 'Admin\CasesController@update']);
    Route::get('cases/delete/{id}',         ['as' => 'admin.cases.delete',          'uses' => 'Admin\CasesController@destroy']);
    Route::get('cases/print',               ['as' => 'admin.cases.print',           'uses' => 'Admin\CasesController@exportAsPrint']);
    Route::get('cases/pdf',                 ['as' => 'admin.cases.pdf',             'uses' => 'Admin\CasesController@exportAsPdf']);
    Route::get('cases/excel',               ['as' => 'admin.cases.excel',           'uses' => 'Admin\CasesController@exportAsExcel']);
    Route::get('cases/print/details/{id}',  ['as' => 'admin.cases.print.details',   'uses' => 'Admin\CasesController@printCaseDetail']);
    Route::get('cases/transfer/{id}',       ['as' => 'admin.cases.transfer',        'uses' => 'Admin\CasesController@transfer']);
    Route::post('cases/transfer/{id}',      ['as' => 'admin.cases.transfer',        'uses' => 'Admin\CasesController@storeTransfer']);

    // For Admin Company
    Route::get('company/list',           ['as' => 'admin.company.list',   'uses' => 'Admin\CompanyController@index']);
    Route::get('company/add',            ['as' => 'admin.company.add',    'uses' => 'Admin\CompanyController@create']);
    Route::post('company/store',         ['as' => 'admin.company.store',  'uses' => 'Admin\CompanyController@store']);
    Route::get('company/edit/{id}',      ['as' => 'admin.company.edit',   'uses' => 'Admin\CompanyController@edit']);
    Route::get('company/view/{id}',      ['as' => 'admin.company.view',   'uses' => 'Admin\CompanyController@view']);
    Route::post('company/update/{id}',   ['as' => 'admin.company.update', 'uses' => 'Admin\CompanyController@update']);
    Route::get('company/delete/{id}',    ['as' => 'admin.company.delete', 'uses' => 'Admin\CompanyController@destroy']);
    Route::get('company/print',          ['as' => 'admin.company.print',  'uses' => 'Admin\CompanyController@exportAsPrint']);
    Route::get('company/pdf',            ['as' => 'admin.company.pdf',    'uses' => 'Admin\CompanyController@exportAsPdf']);
    Route::get('company/excel',          ['as' => 'admin.company.excel',  'uses' => 'Admin\CompanyController@exportAsExcel']);

    // For Admin Case History
    Route::get('cases/history/add/{id}',        ['as' => 'admin.cases.history.add',         'uses' => 'Admin\CasesHistoryController@create']);
    Route::post('cases/history/store',          ['as' => 'admin.cases.history.store',       'uses' => 'Admin\CasesHistoryController@store']);
    Route::get('cases/history/view/{id}',       ['as' => 'admin.cases.history.view',        'uses' => 'Admin\CasesHistoryController@view']);
    Route::get('cases/history/edit/{id}',       ['as' => 'admin.cases.history.edit',        'uses' => 'Admin\CasesHistoryController@edit']);
    Route::post('cases/history/update/{id}',    ['as' => 'admin.cases.history.update',      'uses' => 'Admin\CasesHistoryController@update']);
    Route::get('cases/history/delete/{id}',     ['as' => 'admin.cases.history.delete',      'uses' => 'Admin\CasesHistoryController@destroy']);

    // For Admin Case CaseComment
    Route::get('cases/comment/{id}',            ['as' => 'admin.cases.comment.list',    'uses' => 'Admin\CaseCommentController@index']);
    Route::post('cases/comment/store/{id}',     ['as' => 'admin.cases.comment.store',   'uses' => 'Admin\CaseCommentController@store']);

    // For Admin Case History CaseComment
    Route::get('cases/comment/history/{id}',            ['as' => 'admin.cases.comment-history.list',    'uses' => 'Admin\CaseHistoryCommentController@index']);
    Route::post('cases/comment/history/store/{id}',     ['as' => 'admin.cases.comment-history.store',   'uses' => 'Admin\CaseHistoryCommentController@store']);
});

Route::group(['prefix' => 'technician', 'middleware' => 'auth'], function ()
{
    // For Technician Dashboard
    Route::get('dashboard',     ['as' => 'technician.dashboard',    'uses' => 'Technician\DashboardController@index']);

    // For Technician user
    Route::get('user/edit/{id}',        ['as' => 'technician.user.edit',        'uses' => 'Technician\UserController@edit']);
    Route::post('user/update/{id}',     ['as' => 'technician.user.update',      'uses' => 'Technician\UserController@update']);
    Route::get('user/profile',          ['as' => 'technician.user.profile',     'uses' => 'Technician\UserController@profile']);

    // For Technician Customer
    Route::get('customer',                      ['as' => 'technician.customer',                 'uses' => 'Technician\CustomerController@index']);
    Route::get('customer/add',                  ['as' => 'technician.customer.add',             'uses' => 'Technician\CustomerController@create']);
    Route::post('customer/store',               ['as' => 'technician.customer.store',           'uses' => 'Technician\CustomerController@store']);
    Route::get('customer/view/{id}',            ['as' => 'technician.customer.view',            'uses' => 'Technician\CustomerController@view']);
    Route::get('customer/edit/{id}',            ['as' => 'technician.customer.edit',            'uses' => 'Technician\CustomerController@edit']);
    Route::post('customer/update/{id}',         ['as' => 'technician.customer.update',          'uses' => 'Technician\CustomerController@update']);
    Route::get('customer/delete/{id}',          ['as' => 'technician.customer.delete',          'uses' => 'Technician\CustomerController@destroy']);
    Route::get('customer/profile',              ['as' => 'technician.customer.profile',         'uses' => 'Technician\CustomerController@profile']);
    Route::get('customer/print',                ['as' => 'technician.customer.print',           'uses' => 'Technician\CustomerController@exportAsPrint']);
    Route::get('customer/pdf',                  ['as' => 'technician.customer.pdf',             'uses' => 'Technician\CustomerController@exportAsPdf']);
    Route::get('customer/excel',                ['as' => 'technician.customer.excel',           'uses' => 'Technician\CustomerController@exportAsExcel']);
    Route::get('customer/print/details/{id}',   ['as' => 'technician.customer.print.details',   'uses' => 'Technician\CustomerController@printCustomerDetail']);

    // For Technician Item
    Route::get('items/list',            ['as' => 'technician.items.list',       'uses' => 'Technician\ItemsController@index']);
    Route::get('items/add',             ['as' => 'technician.items.add',        'uses' => 'Technician\ItemsController@create']);
    Route::post('items/store',          ['as' => 'technician.items.store',      'uses' => 'Technician\ItemsController@store']);
    Route::get('items/edit/{id}',       ['as' => 'technician.items.edit',       'uses' => 'Technician\ItemsController@edit']);
    Route::post('items/update/{id}',    ['as' => 'technician.items.update',     'uses' => 'Technician\ItemsController@update']);
    Route::get('items/delete/{id}',     ['as' => 'technician.items.delete',     'uses' => 'Technician\ItemsController@destroy']);
    Route::get('items/print',           ['as' => 'technician.items.print',      'uses' => 'Technician\ItemsController@exportAsPrint']);
    Route::get('items/pdf',             ['as' => 'technician.items.pdf',        'uses' => 'Technician\ItemsController@exportAsPdf']);
    Route::get('items/excel',           ['as' => 'technician.items.excel',      'uses' => 'Technician\ItemsController@exportAsExcel']);

    // For Technician Cases
    Route::get('cases/list',                    ['as' => 'technician.cases.list',               'uses' => 'Technician\CasesController@index']);
    Route::get('cases/add',                     ['as' => 'technician.cases.add',                'uses' => 'Technician\CasesController@create']);
    Route::post('cases/store',                  ['as' => 'technician.cases.store',              'uses' => 'Technician\CasesController@store']);
    Route::get('cases/view/{id}',               ['as' => 'technician.cases.view',               'uses' => 'Technician\CasesController@view']);
    Route::get('cases/edit/{id}',               ['as' => 'technician.cases.edit',               'uses' => 'Technician\CasesController@edit']);
    Route::post('cases/update/{id}',            ['as' => 'technician.cases.update',             'uses' => 'Technician\CasesController@update']);
    Route::get('cases/delete/{id}',             ['as' => 'technician.cases.delete',             'uses' => 'Technician\CasesController@destroy']);
    Route::get('cases/print',                   ['as' => 'technician.cases.print',              'uses' => 'Technician\CasesController@exportAsPrint']);
    Route::get('cases/pdf',                     ['as' => 'technician.cases.pdf',                'uses' => 'Technician\CasesController@exportAsPdf']);
    Route::get('cases/excel',                   ['as' => 'technician.cases.excel',              'uses' => 'Technician\CasesController@exportAsExcel']);
    Route::get('cases/print/details/{id}',      ['as' => 'technician.cases.print.details',      'uses' => 'Technician\CasesController@printCaseDetail']);

    // For Technician Company
    Route::get('company/list',           ['as' => 'technician.company.list',   'uses' => 'Technician\CompanyController@index']);
    Route::get('company/add',            ['as' => 'technician.company.add',    'uses' => 'Technician\CompanyController@create']);
    Route::post('company/store',         ['as' => 'technician.company.store',  'uses' => 'Technician\CompanyController@store']);
    Route::get('company/edit/{id}',      ['as' => 'technician.company.edit',   'uses' => 'Technician\CompanyController@edit']);
    Route::get('company/view/{id}',      ['as' => 'technician.company.view',   'uses' => 'Technician\CompanyController@view']);
    Route::post('company/update/{id}',   ['as' => 'technician.company.update', 'uses' => 'Technician\CompanyController@update']);
    Route::get('company/delete/{id}',    ['as' => 'technician.company.delete', 'uses' => 'Technician\CompanyController@destroy']);
    Route::get('company/print',          ['as' => 'technician.company.print',  'uses' => 'Technician\CompanyController@exportAsPrint']);
    Route::get('company/pdf',            ['as' => 'technician.company.pdf',    'uses' => 'Technician\CompanyController@exportAsPdf']);
    Route::get('company/excel',          ['as' => 'technician.company.excel',  'uses' => 'Technician\CompanyController@exportAsExcel']);

    // For Technician Case History
    Route::get('cases/history/add/{id}',        ['as' => 'technician.cases.history.add',        'uses' => 'Technician\CasesHistoryController@create']);
    Route::post('cases/history/store',          ['as' => 'technician.cases.history.store',      'uses' => 'Technician\CasesHistoryController@store']);
    Route::get('cases/history/view/{id}',       ['as' => 'technician.cases.history.view',       'uses' => 'Technician\CasesHistoryController@view']);
    Route::get('cases/history/edit/{id}',       ['as' => 'technician.cases.history.edit',       'uses' => 'Technician\CasesHistoryController@edit']);
    Route::post('cases/history/update/{id}',    ['as' => 'technician.cases.history.update',     'uses' => 'Technician\CasesHistoryController@update']);
    Route::get('cases/history/delete/{id}',     ['as' => 'technician.cases.history.delete',     'uses' => 'Technician\CasesHistoryController@destroy']);

    // For Technician Case CaseComment
    Route::get('cases/comment/{id}',            ['as' => 'technician.cases.comment.list',       'uses' => 'Technician\CaseCommentController@index']);
    Route::post('cases/comment/store/{id}',     ['as' => 'technician.cases.comment.store',      'uses' => 'Technician\CaseCommentController@store']);

    // For Technician Case History CaseComment
    Route::get('cases/comment/history/{id}',            ['as' => 'technician.cases.comment-history.list',   'uses' => 'Technician\CaseHistoryCommentController@index']);
    Route::post('cases/comment/history/store/{id}',     ['as' => 'technician.cases.comment-history.store',  'uses' => 'Technician\CaseHistoryCommentController@store']);
});

Route::group(['prefix' => 'company', 'middleware' => 'auth'], function ()
{
    // For Company Dashboard
    Route::get('dashboard',     ['as' => 'company.dashboard',    'uses' => 'Company\DashboardController@index']);

    // For Company user
    Route::get('user/edit/{id}',        ['as' => 'company.user.edit',        'uses' => 'Company\UserController@edit']);
    Route::post('user/update/{id}',     ['as' => 'company.user.update',      'uses' => 'Company\UserController@update']);
    Route::get('user/profile',          ['as' => 'company.user.profile',     'uses' => 'Company\UserController@profile']);

    // For Company Cases
    Route::get('cases/list',                    ['as' => 'company.cases.list',               'uses' => 'Company\CasesController@index']);
    Route::get('cases/view/{id}',               ['as' => 'company.cases.view',               'uses' => 'Company\CasesController@view']);
    Route::get('cases/print',                   ['as' => 'company.cases.print',              'uses' => 'Company\CasesController@exportAsPrint']);
    Route::get('cases/pdf',                     ['as' => 'company.cases.pdf',                'uses' => 'Company\CasesController@exportAsPdf']);
    Route::get('cases/excel',                   ['as' => 'company.cases.excel',              'uses' => 'Company\CasesController@exportAsExcel']);
    Route::get('cases/print/details/{id}',      ['as' => 'company.cases.print.details',      'uses' => 'Company\CasesController@printCaseDetail']);
    Route::get('cases/charge/add/{id}',         ['as' => 'company.cases.charge.add',         'uses' => 'Company\CasesController@addCharge']);
    Route::post('cases/charge/store/{id}',      ['as' => 'company.cases.charge.store',       'uses' => 'Company\CasesController@storeCharge']);
    Route::get('cases/charge/edit/{id}',        ['as' => 'company.cases.charge.edit',        'uses' => 'Company\CasesController@editCharge']);
    Route::post('cases/charge/update/{id}',     ['as' => 'company.cases.charge.update',      'uses' => 'Company\CasesController@updateCharge']);

    // For Technician Case History
    Route::get('cases/history/add/{id}',        ['as' => 'company.cases.history.add',        'uses' => 'Company\CasesHistoryController@create']);
    Route::post('cases/history/store',          ['as' => 'company.cases.history.store',      'uses' => 'Company\CasesHistoryController@store']);
    Route::get('cases/history/view/{id}',       ['as' => 'company.cases.history.view',       'uses' => 'Company\CasesHistoryController@view']);
    Route::get('cases/history/edit/{id}',       ['as' => 'company.cases.history.edit',       'uses' => 'Company\CasesHistoryController@edit']);
    Route::post('cases/history/update/{id}',    ['as' => 'company.cases.history.update',     'uses' => 'Company\CasesHistoryController@update']);
    // Route::get('cases/history/delete/{id}',     ['as' => 'company.cases.history.delete',     'uses' => 'Company\CasesHistoryController@destroy']);
    
    // For Company Case CaseComment
    Route::get('cases/comment/{id}',            ['as' => 'company.cases.comment.list',       'uses' => 'Company\CaseCommentController@index']);
    Route::post('cases/comment/store/{id}',     ['as' => 'company.cases.comment.store',      'uses' => 'Company\CaseCommentController@store']);

    // For Company Case History CaseComment
    Route::get('cases/comment/history/{id}',            ['as' => 'company.cases.comment-history.list',   'uses' => 'Company\CaseHistoryCommentController@index']);
    Route::post('cases/comment/history/store/{id}',     ['as' => 'company.cases.comment-history.store',  'uses' => 'Company\CaseHistoryCommentController@store']);

});

Route::group(['prefix' => 'customer', 'middleware' => 'auth'], function ()
{
    // For Customer Dashboard
    Route::get('dashboard',     ['as' => 'customer.dashboard',      'uses' => 'Customer\DashboardController@index']);

    // For Customer User
    Route::get('user/edit/{id}',        ['as' => 'customer.user.edit',      'uses' => 'Customer\UserController@edit']);
    Route::post('user/update/{id}',     ['as' => 'customer.user.update',    'uses' => 'Customer\UserController@update']);
    Route::get('user/profile',          ['as' => 'customer.user.profile',   'uses' => 'Customer\UserController@profile']);

    // For Customer Account
    Route::get('account',                               ['as' => 'customer.account',                        'uses' => 'Customer\CustomerController@index']);
    Route::get('case/history/view/{id}',                ['as' => 'customer.case.history.view',              'uses' => 'Customer\CustomerController@viewCaseHistory']);
    Route::get('case/comment/view/{id}',                ['as' => 'customer.case.comment.view',              'uses' => 'Customer\CustomerController@viewCaseComment']);
    Route::post('case/comment/store/{id}',              ['as' => 'customer.case.comment.store',             'uses' => 'Customer\CustomerController@CaseCommentStore']);
    Route::get('case/history/comment/view/{id}',        ['as' => 'customer.case.history.comment.view',      'uses' => 'Customer\CustomerController@viewCaseHistoryComment']);
    Route::post('case/history/comment/store/{id}',      ['as' => 'customer.case.history.comment.store',     'uses' => 'Customer\CustomerController@CaseHistoryCommentStore']);

});











