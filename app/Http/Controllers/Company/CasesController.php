<?php

namespace App\Http\Controllers\Company;

use App\Models\Cases;
use App\Models\Items;
use Auth;
use Gate;
use DB;
use Illuminate\Foundation\Auth\User;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Company\Cases\AddChargeFormValidation;
use App\Http\Requests\Company\Cases\UpdateChargeFormValidation;


class CasesController extends CompanyBaseController {

    protected $view_path = 'company.cases';
    protected $base_route = 'company.cases';
    protected $model;
    protected $image_path;

    public function __construct()
    {
        $this->image_path = config('global.path.frontend.image') . 'item';
    }

    public function index()
    {
        $user = Auth::user();

        $data = [];
        $data['rows'] = DB::select(DB::raw(" 
                          SELECT c.*, i.item_type, u.fullname, u.mobile_number, u.home_number FROM cases AS c INNER  JOIN 
                          items AS i ON i.id = c.item_id INNER JOIN 
                          users AS u ON c.customer_id = u.id 
                          WHERE c.handled_by = '$user->office_name'
                          ORDER BY c.id DESC "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];

        $data['row'] = DB::select(DB::raw(" SELECT c.*, i.item_type, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id WHERE c.id = '$id' "));

        $data['case_comments'] = DB::select(DB::raw(" SELECT cm.case_comment_desc, cm.created_at, u.office_name, u.fullname, u.role FROM case_comment AS cm 
                                            INNER JOIN users AS u ON cm.comment_id = u.id 
                                            INNER JOIN cases AS c ON cm.case_id = c.id WHERE c.id = '$id' 
                                            ORDER BY cm.created_at DESC LIMIT 1"));

        $data['cases_history'] = DB::select(DB::raw(" SELECT ch.* FROM cases_history AS ch
                                            INNER JOIN cases AS c ON c.id = ch.case_id WHERE c.id = '$id' "));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = DB::select(DB::raw(" SELECT c.*, i.item_type, i.item_description, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id ORDER BY c.id"));

        return view(parent::loadDefaultVars($this->view_path . '.casesPrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = DB::select(DB::raw(" SELECT c.*, i.item_type, i.item_description, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id ORDER BY c.id"));

        $pdf = PDF::loadView($this->view_path . '.casesPdf', compact('data'));

        return $pdf->download('CasesListPDF.pdf');
    }

    public function exportAsExcel()
    {
        $cases = Cases::join('users', 'users.id', '=', 'cases.customer_id')
            ->join('items', 'items.id', '=', 'cases.item_id')
            ->select('cases.id', 'cases.case_date', 'users.fullname', 'cases.case_code', 'items.item_type', 'cases.case_name', 'cases.item_brand', 'cases.item_model',
                'cases.item_specs', 'cases.minimum_charge')
            ->orderBy('cases.id', 'ASC')
            ->get();


        $casesArray = [];

        // Define the Excel spreadsheet headers
        $casesArray[] = ['S.N.', 'Entry Date', 'Customer Name', 'Case Code', 'Item Name', 'Case Name', 'Item Brand', 'Item Model', 'Item Specs', 'Minimum Charge'];

        // Convert each member of the returned collection into an array,
        // and append it to the student array.
        foreach ($cases as $case)
        {
            $casesArray[] = $case->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Cases Information', function ($excel) use ($casesArray)
        {

            // Build the spreadsheet, passing in the student array
            $excel->sheet('sheet1', function ($sheet) use ($casesArray)
            {
                $sheet->fromArray($casesArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function create()
    {
        $data = [];

        $data['customers'] = User::select('id', 'fullname')->where('role', '=', 'customer')->get();

        $data['items'] = Items::select('id', 'item_type')->orderBy('item_order', 'ASC')->get();

        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    public function store(AddCaseFormValidation $request)
    {
        Cases::create([
            'customer_id'    => $request->get('customer_id'),
            'case_code'      => AppHelper::getCaseCode(),
            'case_date'      => $request->get('case_date'),
            'item_id'        => $request->get('item_id'),
            'case_name'      => $request->get('case_name'),
            'item_brand'     => $request->get('item_brand'),
            'item_model'     => $request->get('item_model'),
            'item_specs'     => $request->get('item_specs'),
            'minimum_charge' => $request->get('minimum_charge'),
            'created_at'     => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'     => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $case = Cases::select('case_code')->where('case_date', $request->get('case_date'))->get();

        AppHelper::flash('success', 'Record created successfully with case code'.' <b>'. $case['0']->case_code. '</b>');

        return redirect()->route($this->base_route . '.list');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];

        $data['customers'] = User::select('id', 'fullname')->where('role', '=', 'customer')->get();

        $data['items'] = Items::select('id', 'item_type')->orderBy('item_order', 'ASC')->get();

        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateCaseFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = $this->model;

        $data->update([
            'customer_id'    => $request->get('customer_id'),
            'case_date'      => $request->get('case_date'),
            'item_id'        => $request->get('item_id'),
            'case_name'      => $request->get('case_name'),
            'item_brand'     => $request->get('item_brand'),
            'item_model'     => $request->get('item_model'),
            'item_specs'     => $request->get('item_specs'),
            'minimum_charge' => $request->get('minimum_charge'),
            'updated_at'     => Carbon::now()->format('Y-m-d H:i:s'),

        ]);

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route . '.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route . '.list');
        }

        Cases::destroy($id);

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route . '.list');
    }

    public function printCaseDetail($id)
    {
        $data = [];
        $data['case_details'] = Cases::join('users', 'users.id', '=', 'cases.customer_id')
            ->join('items', 'items.id', '=', 'cases.item_id')
            ->select('cases.id', 'cases.case_date', 'users.fullname',
                'cases.case_code', 'items.item_type', 'cases.case_name', 'cases.item_brand', 'cases.item_model',
                'cases.item_specs', 'cases.minimum_charge')
            ->where('cases.id', $id)
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.printDetails'), compact('data'));
    }

    public function addCharge($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars('company.cases.addCharge'), compact('data'));
    }

    public function storeCharge(AddChargeFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        DB::table('cases')
            ->where('id', $id)
            ->update([
                'vendor_charge' => $request->get('vendor_charge'),
            ]);

        AppHelper::flash('success', 'Vendor Charge has been successfully added.');

        return redirect()->route($this->base_route . '.view', ['id' => $id]);
    }

    public function editCharge($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars('company.cases.editCharge'), compact('data'));
    }

    public function updateCharge(UpdateChargeFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        DB::table('cases')
            ->where('id', $id)
            ->update([
                'vendor_charge' => $request->get('vendor_charge'),
            ]);

        AppHelper::flash('success', 'Vendor Charge has been successfully added.');

        return redirect()->route($this->base_route . '.view', ['id' => $id]);
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Cases::find($id);

        return $this->model;
    }
}
