<?php

namespace App\Http\Controllers\Company;

use App\Models\Cases;
use App\Models\CasesHistory;
use Auth;
use Gate;
use DB;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Company\Cases\AddCaseHistoryFormValidation;
use App\Http\Requests\Company\Cases\UpdateCaseHistoryFormValidation;

class CasesHistoryController extends CompanyBaseController {

    protected $view_path = 'company.cases';
    protected $base_route = 'company.cases';
    protected $model;
    protected $image_path;

    public function __construct()
    {
        $this->image_path = config('global.path.frontend.image') . 'item';
    }

    public function create($id)
    {
        $data = [];

        $data['cases'] = Cases::join('users', 'users.id', '=', 'cases.customer_id')
            ->select('cases.id', 'cases.case_name', 'users.fullname')
            ->where('cases.id', '=', $id)
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.history.create'), compact('data'));
    }

    public function store(AddCaseHistoryFormValidation $request)
    {

        if (!file_exists(config('global.path.frontend.image') . 'item'))
        {
            mkdir(config('global.path.frontend.image') . 'item');
        }

        If ($image_one = $request->file('image_one'))
        {
            $image_one_name = rand(5558, 9999) . '_' . $image_one->getClientOriginalName();
            $image_one->move($this->image_path, $image_one_name);
        }

        if ($image_two = $request->file('image_two'))
        {
            $image_two_name = rand(5558, 9999) . '_' . $image_two->getClientOriginalName();
            $image_two->move($this->image_path, $image_two_name);
        }

        $data = [];
        $data['row'] = CasesHistory::create([
            'case_id'           => $request->get('case_id'),
            'case_history_date' => $request->get('case_history_date'),
            'item_status'       => $request->get('item_status'),
            'image_one'         => isset($image_one) ? $image_one_name : '',
            'image_two'         => isset($image_two) ? $image_two_name : '',
            'description'       => $request->get('description'),
            'etr'               => $request->get('etr'),
            'charge'            => $request->get('charge'),
            'remarks'           => $request->get('remarks'),
            'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route . '.view', ['id' => $request->get('case_id')]);
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }
        $data = [];

        $data['cases'] = Cases::join('users', 'users.id', '=', 'cases.customer_id')
            ->join('cases_history', 'cases_history.case_id', '=', 'cases.id')
            ->select('cases.id', 'cases.case_name', 'users.fullname')
            ->where('cases_history.id', '=', $id)
            ->get();

        $data['row'] = CasesHistory::find($id);

        return view(parent::loadDefaultVars($this->view_path . '.history.edit'), compact('data'));
    }

    public function update(UpdateCaseHistoryFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        if (!file_exists(config('global.path.frontend.image') . 'item'))
        {
            mkdir(config('global.path.frontend.image') . 'item');
        }

        if ($image_one = $request->file('image_one'))
        {
            $image_one_name = rand(5558, 9999) . '_' . $image_one->getClientOriginalName();
            if ($image_one->move($this->image_path, $image_one_name))
            {

                if ($this->model->image_one && file_exists($this->image_path . DIRECTORY_SEPARATOR . $this->model->image_one))
                {
                    unlink($this->image_path . DIRECTORY_SEPARATOR . $this->model->image_one);
                }
                // remove old image
                $this->model->image_one = $image_one_name;
            }
        }

        if ($image_two = $request->file('image_two'))
        {
            $image_two_name = rand(5558, 9999) . '_' . $image_two->getClientOriginalName();
            if ($image_two->move($this->image_path, $image_two_name))
            {

                if ($this->model->image_two && file_exists($this->image_path . DIRECTORY_SEPARATOR . $this->model->image_two))
                {
                    unlink($this->image_path . DIRECTORY_SEPARATOR . $this->model->image_two);
                }

                // remove old image
                $this->model->image_two = $image_two_name;
            }
        }

        $data = $this->model;

        $data->update([
            'case_id'           => $request->get('case_id'),
            'case_history_date' => $request->get('case_history_date'),
            'item_status'       => $request->get('item_status'),
            'image_one'         => isset($image_one) ? $image_one_name : '',
            'image_two'         => isset($image_two) ? $image_two_name : '',
            'description'       => $request->get('description'),
            'etr'               => $request->get('etr'),
            'charge'            => $request->get('charge'),
            'remarks'           => $request->get('remarks'),
            'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),

        ]);

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route . '.view', ['id' => $request->get('case_id')]);
    }

    public function view($id)
    {
        $data = [];

        $data['row'] = DB::select(DB::raw(" SELECT ch.*, i.item_type, i.item_description, c.id AS case_id, c.case_code, c.case_name, c.item_brand, c.item_model, c.item_specs, c.minimum_charge, u.fullname FROM cases_history AS ch 
                                            INNER JOIN cases AS c ON c.id = case_id
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id WHERE ch.id = '$id' "));

        return view(parent::loadDefaultVars($this->view_path . '.history.view'), compact('data'));
    }
    
    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = CasesHistory::find($id);

        return $this->model;
    }
}
