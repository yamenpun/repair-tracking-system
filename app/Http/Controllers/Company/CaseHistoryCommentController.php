<?php

namespace App\Http\Controllers\Company;

use App\Http\Requests\Technician\CaseHistoryComment\AddFormValidation;
use App\Models\CaseComment;
use App\Models\CaseHistoryComment;
use Auth;
use Gate;
use Carbon;
use DB;
use PDF;
use Excel;
use AppHelper;
use App\Http\Requests;


class CaseHistoryCommentController extends CompanyBaseController {

    protected $view_path = 'company.case-history-comment';
    protected $base_route = 'company.case-history-comment';
    protected $model;

    public function index($id)
    {
        $data = [];
        $data['row'] = DB::select(DB::raw(" SELECT ch.*, c.case_name FROM cases_history AS ch 
                                          INNER JOIN cases AS c ON c.id = ch.case_id
                                          WHERE ch.id = '$id' "));

        $data['case_history_comments'] = DB::select(DB::raw(" SELECT c.*, u.fullname, u.office_name, u.role FROM case_history_comment AS c
                                            INNER JOIN cases_history AS ch ON ch.id = c.case_history_id
                                            INNER JOIN users AS u ON u.id = c.comment_id
                                             WHERE ch.id = '$id' ORDER BY c.created_at ASC"));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function store(AddFormValidation $request, $id)
    {
        $user = Auth::user();

        CaseHistoryComment::create([
            'case_history_id'           => $id,
            'comment_id'                => $user->id,
            'case_history_comment_desc' => $request->get('case_history_comment_desc'),
            'created_at'                => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'                => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $data = [];
        $data['row'] = DB::select(DB::raw(" SELECT ch.*, c.case_name FROM cases_history AS ch 
                                          INNER JOIN cases AS c ON c.id = ch.case_id
                                          WHERE ch.id = '$id' "));

        AppHelper::flash('success', 'Message has been send successfully.');

        return redirect()->route('technician.cases.comment-history.list', ['id' => $id]);
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = CaseComment::find($id);

        return $this->model;
    }
}
