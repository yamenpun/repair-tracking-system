<?php

namespace App\Http\Controllers\Company;

use App\Http\Requests\Technician\Items\AddFormValidation;
use App\Http\Requests\Technician\Items\UpdateFormValidation;
use App\Models\Items;
use Auth;
use Gate;
use DB;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;

class ItemsController extends CompanyBaseController {

    protected $view_path = 'company.items';
    protected $base_route = 'company.items';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = DB::select(DB::raw(" SELECT * FROM items ORDER BY id ASC "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = Items::select('id', 'item_name', 'item_description', 'item_order', 'created_at', 'updated_at')->get();

        return view(parent::loadDefaultVars($this->view_path . '.itemsPrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = Items::select('id', 'item_name', 'item_description', 'item_order', 'created_at', 'updated_at')->get();

        $pdf = PDF::loadView($this->view_path . '.itemsPdf', compact('data'));

        return $pdf->download('itemsPDF.pdf');
    }

    public function exportAsExcel()
    {
        $items = Items::select('id', 'item_name', 'item_description', 'item_order', 'created_at', 'updated_at')->get();

        $itemsArray = [];

        // Define the Excel spreadsheet headers
        $itemsArray[] = ['S.N.', 'Items Name', 'Items Description', 'Items Order', 'Created At', 'Updated At'];

        // Convert each member of the returned collection into an array,
        // and append it to the college array.
        foreach ($items as $item)
        {
            $itemsArray[] = $item->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Items Information', function ($excel) use ($itemsArray)
        {

            // Build the spreadsheet, passing in the college array
            $excel->sheet('sheet1', function ($sheet) use ($itemsArray)
            {
                $sheet->fromArray($itemsArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        Items::create([
            'item_name'        => $request->get('item_name'),
            'item_description' => $request->get('item_description'),
            'item_order'       => $request->get('item_order'),
            'created_at'       => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'       => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route . '.list');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = $this->model;

        $data->update([
            'item_name'        => $request->get('item_name'),
            'item_description' => $request->get('item_description'),
            'item_order'       => $request->get('item_order'),
            'updated_at'       => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route . '.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route . '.list');
        }

        Items::destroy($id);

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route . '.list');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Items::find($id);

        return $this->model;
    }
}
