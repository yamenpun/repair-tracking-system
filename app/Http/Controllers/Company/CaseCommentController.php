<?php

namespace App\Http\Controllers\Company;

use App\Http\Requests\Technician\CaseComment\AddFormValidation;
use App\Models\CaseComment;
use Auth;
use Gate;
use DB;
use Carbon;
use PDF;
use Excel;
use AppHelper;
use App\Http\Requests;


class CaseCommentController extends CompanyBaseController {

    protected $view_path = 'company.case-comment';
    protected $base_route = 'company.case-comment';
    protected $model;

    public function index($id)
    {
        $data = [];
        $data['row'] = DB::select(DB::raw(" SELECT c.*, i.item_type, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id WHERE c.id = '$id' "));

        $data['case_comments'] = DB::select(DB::raw(" SELECT cc.*, u.role, u.office_name, u.fullname FROM case_comment AS cc
                                            INNER JOIN cases AS c ON c.id = cc.case_id
                                            INNER JOIN users AS u ON u.id = cc.comment_id
                                            WHERE c.id = '$id' ORDER BY cc.created_at ASC"));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function store(AddFormValidation $request, $id)
    {
        $user = Auth::user();

        CaseComment::create([
            'case_id'           => $id,
            'comment_id'        => $user->id,
            'case_comment_desc' => $request->get('case_comment_desc'),
            'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $data = [];
        $data['row'] = DB::select(DB::raw(" SELECT c.*, i.item_type, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id WHERE c.id = '$id' "));

        AppHelper::flash('success', 'Message has been send successfully.');

        return redirect()->route('technician.cases.comment.list', ['id' => $id]);
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = CaseComment::find($id);

        return $this->model;
    }
}
