<?php
namespace App\Http\Controllers\Company;


use App\Http\Controllers\AppBaseController;
use View;

class CompanyBaseController extends AppBaseController
{
    protected $user;

    public function __construct()
    {
        parent::__construct();
        $this->user = auth()->user();
    }

    protected function loadDefaultVars($view_path)
    {
        View::composer($view_path, function ($view) use ($view_path) {

            $view->with('view_path', parent::makeViewFolderPath($view_path));
            $view->with('base_route', $this->base_route);
            $view->with('trans_path', $this->makeViewFolderPath($view_path));

        });

        return $view_path;
    }

    protected function getArrayByKey(Collection $data, $key)
    {
        $tmp = [];
        foreach ($data as $item) {
            $tmp[] = $item->$key;
        }

        return $tmp;
    }
    
}