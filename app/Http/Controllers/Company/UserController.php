<?php

namespace App\Http\Controllers\Company;

use Auth;
use Gate;
use App\User;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Company\User\AddFormValidation;
use App\Http\Requests\Company\User\UpdateFormValidation;


class UserController extends CompanyBaseController {

    protected $view_path = 'company.user';
    protected $base_route = 'company.user';
    protected $model;

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        $data = $this->model;

        $data->update([
                'password'    => bcrypt($request->get('password')),
            ]);

        AppHelper::flash('success', 'Password updated successfully.');

        return redirect()->route('technician.customer');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route);
        }
        User::destroy($id);

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route);
    }

    public function profile()
    {
        $data = [];
        $data['rows'] = Auth::user();

        return view(parent::loadDefaultVars($this->view_path . '.profile'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = User::find($id);

        return $this->model;
    }
}
