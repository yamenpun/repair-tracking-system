<?php

namespace App\Http\Controllers;

class HomeController extends AppBaseController
{

    public function index()
    {
        if(auth()->check()){

            $user = auth()->user();

            if ($user->role == 'admin') {
                return redirect()->route('admin.dashboard');
            } elseif($user->role == 'technician') {
                return redirect()->route('technician.dashboard');
            }elseif ($user->role == 'customer'){
                return redirect()->route('customer.dashboard');
            }
        }

        return view('auth.login');
    }
}
