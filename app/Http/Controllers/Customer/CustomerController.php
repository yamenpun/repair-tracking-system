<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests\Customer\Account\AddCaseCommentValidation;
use App\Http\Requests\Customer\Account\AddCaseHistoryCommentValidation;
use App\Models\CaseComment;
use App\Models\CaseHistoryComment;
use App\Models\Cases;
use App\User;
use Auth;
use Gate;
use DB;
use Carbon;
use PDF;
use Excel;
use AppHelper;
use App\Http\Requests;


class CustomerController extends CustomerBaseController {

    protected $view_path = 'customer.account';
    protected $base_route = 'customer.account';
    protected $model;

    public function index()
    {
        $data = [];
        $user = Auth::user();
        $data['row'] = User::select('fullname', 'username', 'email', 'mobile_number', 'home_number', 'address', 'status')->where('id', '=', $user->id)->get();
        $data['cases'] = DB::select(DB::raw("SELECT c.* FROM cases AS c INNER JOIN users AS u ON u.id = c.customer_id WHERE u.id = '$user->id' "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function viewCaseHistory($id)
    {

        $data = [];

        $data['case'] = Cases::select('case_name')->where('id', $id)->get();

        $data['case_history'] = DB::select(DB::raw(" SELECT ch.*, i.item_name, i.item_description, c.id AS case_id, c.case_code, c.case_name, c.item_brand, c.item_model, c.item_specs, c.minimum_charge, u.fullname FROM cases_history AS ch 
                                            INNER JOIN cases AS c ON c.id = case_id
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id WHERE c.id = '$id' "));

        return view(parent::loadDefaultVars($this->view_path . '.viewCaseHistory'), compact('data'));
    }

    public function viewCaseComment($id)
    {
        $data = [];
        $data['row'] = DB::select(DB::raw(" SELECT c.*, i.item_name, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id WHERE c.id = '$id' "));

        $data['case_comments'] = DB::select(DB::raw(" SELECT cc.*, u.fullname, u.office_name, u.role FROM case_comment AS cc
                                            INNER JOIN cases AS c ON c.id = cc.case_id
                                            INNER JOIN users AS u ON u.id = cc.comment_id
                                             WHERE c.id = '$id' ORDER BY cc.created_at ASC"));

        return view(parent::loadDefaultVars($this->view_path . '.viewCaseComment'), compact('data'));
    }

    public function CaseCommentStore(AddCaseCommentValidation $request, $id)
    {
        $user = Auth::user();

        CaseComment::create([
            'case_id'           => $id,
            'comment_id'        => $user->id,
            'case_comment_desc' => $request->get('case_comment_desc'),
            'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $data = [];
        $data['row'] = DB::select(DB::raw(" SELECT c.*, i.item_name, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id WHERE c.id = '$id' "));

        AppHelper::flash('success', 'Message has been send successfully.');

        return redirect()->route('customer.case.comment.view', ['id' => $id]);
    }

    public function viewCaseHistoryComment($id)
    {
        $data = [];
        $data['row'] = DB::select(DB::raw(" SELECT ch.*, c.id as case_id, c.case_name FROM cases_history AS ch 
                                          INNER JOIN cases AS c ON c.id = ch.case_id
                                          WHERE ch.id = '$id' "));

        $data['case_history_comments'] = DB::select(DB::raw(" SELECT c.*, u.role FROM case_history_comment AS c
                                            INNER JOIN cases_history AS ch ON ch.id = c.case_history_id
                                            INNER JOIN users AS u ON u.id = c.comment_id
                                             WHERE ch.id = '$id' ORDER BY c.created_at ASC"));

        return view(parent::loadDefaultVars($this->view_path . '.viewCaseHistoryComment'), compact('data'));
    }

    public function CaseHistoryCommentStore(AddCaseHistoryCommentValidation $request, $id)
    {
        $user = Auth::user();

        CaseHistoryComment::create([
            'case_history_id'           => $id,
            'comment_id'                => $user->id,
            'case_history_comment_desc' => $request->get('case_history_comment_desc'),
            'created_at'                => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'                => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $data = [];
        $data['row'] = DB::select(DB::raw(" SELECT ch.*, c.case_name FROM cases_history AS ch 
                                          INNER JOIN cases AS c ON c.id = ch.case_id
                                          WHERE ch.id = '$id' "));

        AppHelper::flash('success', 'Message has been send successfully.');

        return redirect()->route('customer.case.history.comment.view', ['id' => $id]);
    }


    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = User::find($id);

        return $this->model;
    }
}
