<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests;

class DashboardController extends CustomerBaseController
{
	protected $view_path = 'customer.dashboard';
    protected $base_route = 'customer.dashboard';

    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }
}
