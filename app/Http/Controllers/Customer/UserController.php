<?php

namespace App\Http\Controllers\Customer;

use Auth;
use Gate;
use App\User;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Customer\User\UpdateFormValidation;


class UserController extends CustomerBaseController {

    protected $view_path = 'customer.user';
    protected $base_route = 'customer.user';
    protected $model;

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        $data = $this->model;

        $data->update([
                'password'      => bcrypt($request->get('password')),
                'role'          => 'customer',
            ]);

        AppHelper::flash('success', 'Password updated successfully.');

        return redirect()->route('customer.account');
    }

    public function profile()
    {
        $data = [];
        $data['rows'] = Auth::user();

        return view(parent::loadDefaultVars($this->view_path . '.profile'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = User::find($id);

        return $this->model;
    }
}
