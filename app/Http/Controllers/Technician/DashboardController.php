<?php

namespace App\Http\Controllers\Technician;

use App\Http\Requests;
use App\Models\Cases;
use App\Models\CasesHistory;
use App\User;

class DashboardController extends TechnicianBaseController
{
	protected $view_path = 'technician.dashboard';
    protected $base_route = 'technician.dashboard';

    public function index()
    {
        $data = [];
        $data['no_of_customers'] = User::where('role', 'customer')->count();
        $data['no_of_cases'] = Cases::count();
        $data['no_of_case_history'] = CasesHistory::count();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }
}
