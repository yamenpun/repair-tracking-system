<?php

namespace App\Http\Controllers\Technician;

use App\Http\Requests\Admin\Company\AddFormValidation;
use App\Http\Requests\Admin\Company\UpdateFormValidation;
use App\User;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;
use PDF;
use Excel;
use AppHelper;
use App\Http\Requests;

class CompanyController extends TechnicianBaseController {

    protected $view_path = 'technician.company';
    protected $base_route = 'technician.company';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = User::select('id', 'fullname', 'username', 'email', 'office_name', 'mobile_number', 'home_number', 'address')->where('role', '=', 'company')->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];
        $data['row'] = User::find($id);
        $data['cases'] = DB::select(DB::raw("SELECT c.* FROM cases AS c INNER JOIN users AS u ON u.id = c.customer_id WHERE u.id = '$id' "));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = DB::select(DB::raw(" SELECT * FROM users WHERE role = 'company' ORDER BY id DESC "));

        return view(parent::loadDefaultVars($this->view_path . '.vendorPrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = DB::select(DB::raw(" SELECT * FROM users WHERE role = 'company' ORDER BY id DESC "));

        $pdf = PDF::loadView($this->view_path . '.vendorPdf', compact('data'));

        return $pdf->download('VendorsListPDF.pdf');
    }

    public function exportAsExcel()
    {
        $vendors = User::select('users.id', 'users.office_name', 'users.fullname', 'users.username', 'users.email',  'users.mobile_number',
            'users.home_number', 'users.address', 'users.created_at', 'users.updated_at')
            ->where('users.role', '=', 'company')
            ->orderBy('users.id', 'ASC')
            ->get();


        $vendorsArray = [];

        // Define the Excel spreadsheet headers
        $vendorsArray[] = ['S.N.', 'Vendor Name', 'Contact Person', 'Username', 'Email', 'Office Name', 'Mobile Number', 'Home Number', 'Address', 'Created At', 'Updated At'];

        // Convert each member of the returned collection into an array,
        // and append it to the vendor array.
        foreach ($vendors as $vendor)
        {
            $vendorsArray[] = $vendor->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Customers Information', function ($excel) use ($vendorsArray)
        {

            // Build the spreadsheet, passing in the vendor array
            $excel->sheet('sheet1', function ($sheet) use ($vendorsArray)
            {
                $sheet->fromArray($vendorsArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        User::create([
            'office_name'   => $request->get('office_name'),
            'fullname'      => $request->get('fullname'),
            'username'      => $request->get('username'),
            'password'      => bcrypt($request->get('password')),
            'mobile_number' => $request->get('mobile_number'),
            'home_number'   => $request->get('home_number'),
            'address'       => $request->get('address'),
            'email'         => $request->get('email'),
            'status'        => '1',
            'role'          => 'company',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        $data = $this->model;

        if ($request->get('password') === '')
        {
            $data->update([
                'fullname'      => $request->get('fullname'),
                'username'      => $request->get('username'),
                'email'         => $request->get('email'),
                'password'      => $data->password,
                'office_name'   => $request->get('office_name'),
                'mobile_number' => $request->get('mobile_number'),
                'home_number'   => $request->get('home_number'),
                'address'       => $request->get('address'),
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        } else
        {
            $data->update([
                'fullname'      => $request->get('fullname'),
                'username'      => $request->get('username'),
                'email'         => $request->get('email'),
                'password'      => bcrypt($request->get('password')),
                'office_name'   => $request->get('office_name'),
                'mobile_number' => $request->get('mobile_number'),
                'home_number'   => $request->get('home_number'),
                'address'       => $request->get('address'),
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route);
        }
        User::destroy($id);

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = User::find($id);

        return $this->model;
    }
}
