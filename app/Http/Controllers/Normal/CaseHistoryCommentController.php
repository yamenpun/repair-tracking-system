<?php

namespace App\Http\Controllers\Normal;

use App\Http\Requests\Normal\CaseHistoryComment\AddFormValidation;
use App\Models\CaseComment;
use App\Models\CaseHistoryComment;
use App\Models\Cases;
use Auth;
use Gate;
use Session;
use DB;
use Carbon;
use PDF;
use Excel;
use AppHelper;
use App\Http\Requests;


class CaseHistoryCommentController extends NormalBaseController {

    private $view_path = 'normal.case-history-comment';
    protected $base_route = 'normal.case-history-comment';
    protected $model;

    public function __construct()
    {
        parent::__construct();
    }

    public function showCaseHistoryComment($id)
    {
        if(Session::get('session_code'))
        {
            $data = [];
            $data['row'] = DB::select(DB::raw(" SELECT ch.*, c.case_name FROM cases_history AS ch 
                                          INNER JOIN cases AS c ON c.id = ch.case_id
                                          WHERE ch.id = '$id' "));

            $data['case_history_comments'] = DB::select(DB::raw(" SELECT c.*, u.role FROM case_history_comment AS c
                                            INNER JOIN cases_history AS ch ON ch.id = c.case_history_id
                                            INNER JOIN users AS u ON u.id = c.comment_id
                                             WHERE ch.id = '$id' ORDER BY c.created_at ASC"));

            return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));

        } else{
            
            return redirect()->route('normal.code-login-form');
        }

    }

    public function storeCaseHistoryComment(AddFormValidation $request, $id)
    {
        if(Session::get('session_code'))
        {
            $case_code = Session::get('session_code');

            $user = Cases::select('customer_id')->where('case_code', $case_code)->get();

            CaseHistoryComment::create([
                'case_history_id'           => $id,
                'comment_id'                => $user['0']->customer_id,
                'case_history_comment_desc' => $request->get('case_history_comment_desc'),
                'created_at'                => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'                => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            $data = [];
            $data['row'] = DB::select(DB::raw(" SELECT ch.*, c.case_name FROM cases_history AS ch 
                                          INNER JOIN cases AS c ON c.id = ch.case_id
                                          WHERE ch.id = '$id' "));

            AppHelper::flash('success', 'Message has been send successfully.');

            return redirect()->route('normal.case.history.comment', ['id' => $id]);

        } else{

            return redirect()->route('normal.code-login-form');
        }

    }

}
