<?php

namespace App\Http\Controllers\Normal;

use App\Http\Requests\Normal\CodeLoginValidation;
use App\Http\Requests\Normal\NameLoginValidation;
use App\Models\Cases;
use App\User;
use Auth;
use Gate;
use Session;
use DB;
use PDF;
use Excel;
use AppHelper;
use App\Http\Requests;


class CustomerController extends NormalBaseController {

    private $view_path = 'normal.login';
    protected $base_route = 'normal.login';
    protected $model;

    public function __construct()
    {
        parent::__construct();
    }

    public function showCaseCodeLoginForm()
    {
        return view(parent::loadDefaultVars($this->view_path . '.caseCodeLogin'));
    }

    public function caseCodeLogin(CodeLoginValidation $request)
    {
        $case_codes = Cases::select('case_code')->where('case_code', $request->get('case_code'))->get();

        if($case_codes->count() > 0)
        {
            foreach ($case_codes as $case_code)
            {
                if ($case_code->case_code === $request->get('case_code'))
                {
                    Session::set('session_code', $request->get('case_code'));

                    return redirect()->route('normal.name-login-form');

                } else
                {
                    AppHelper::flash('danger', 'Please enter a valid case code');

                    return redirect()->route('normal.code-login-form');
                }
            }
        } else {

            AppHelper::flash('danger', 'Please enter a valid case code');

            return redirect()->route('normal.code-login-form');
        }

    }

    public function showNameLoginForm()
    {
        if(Session::get('session_code'))
        {
            return view(parent::loadDefaultVars($this->view_path . '.nameLogin'));
        } else{
            return redirect()->route('normal.code-login-form');
        }

    }

    public function nameLogin(NameLoginValidation $request)
    {
        $case_code = Session::get('session_code');

        $users = User::join('cases', 'users.id', '=', 'cases.customer_id')->select('users.fullname')
            ->where('cases.case_code', '=', $case_code)
            ->get();

        if ($users[0]->fullname === $request->get('fullname'))
        {
            return redirect()->route('normal.dashboard');

        } else
        {
            AppHelper::flash('danger', 'Please enter a valid Full Name');

            return redirect()->route('normal.name-login-form');
        }

    }

    public function dashboard()
    {
        if(Session::get('session_code'))
        {
            $data = [];
            $session_code = Session::get('session_code');
            $data['row'] = DB::select(DB::raw(" SELECT c.*, i.item_name FROM cases AS c 
                                 INNER JOIN items AS i ON i.id = c.item_id
                                 WHERE c.case_code = '$session_code' "));

            $data['case_history'] = DB::select(DB::raw(" SELECT ch.* FROM cases_history AS ch 
                                 INNER JOIN cases AS c ON c.id = ch.case_id
                                 WHERE c.case_code = '$session_code' "));

            return view(parent::loadDefaultVars('normal.dashboard.index'), compact('data'));
        } else{
            return redirect()->route('normal.code-login-form');
        }

    }

    public function logout()
    {
        Session::forget('session_code');

        return redirect()->route('normal.code-login-form');
    }

}
