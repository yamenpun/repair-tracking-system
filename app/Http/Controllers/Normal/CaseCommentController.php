<?php

namespace App\Http\Controllers\Normal;

use App\Http\Requests\Normal\CaseComment\AddFormValidation;
use App\Models\CaseComment;
use App\Models\Cases;
use Auth;
use Gate;
use Session;
use DB;
use Carbon;
use PDF;
use Excel;
use AppHelper;
use App\Http\Requests;


class CaseCommentController extends NormalBaseController {

    private $view_path = 'normal.case-comment';
    protected $base_route = 'normal.case-comment';
    protected $model;

    public function __construct()
    {
        parent::__construct();
    }

    public function showCaseComment($id)
    {
        if(Session::get('session_code'))
        {
            $data = [];
            $data['row'] = DB::select(DB::raw(" SELECT c.*, i.item_name, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id WHERE c.id = '$id' "));

            $data['case_comments'] = DB::select(DB::raw(" SELECT cc.*, u.role, u.fullname FROM case_comment AS cc
                                            INNER JOIN cases AS c ON c.id = cc.case_id
                                            INNER JOIN users AS u ON u.id = cc.comment_id
                                            WHERE c.id = '$id' ORDER BY cc.created_at ASC"));

            return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));

        } else{
            
            return redirect()->route('normal.code-login-form');
        }

    }

    public function storeCaseComment(AddFormValidation $request, $id)
    {
        if(Session::get('session_code'))
        {
            $case_code = Session::get('session_code');

            $user = Cases::select('customer_id')->where('case_code', $case_code)->get();

            CaseComment::create([
                'case_id'           => $id,
                'comment_id'        => $user['0']->customer_id,
                'case_comment_desc' => $request->get('case_comment_desc'),
                'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            $data = [];
            $data['row'] = DB::select(DB::raw(" SELECT c.*, i.item_name, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id WHERE c.id = '$id' "));

            AppHelper::flash('success', 'Message has been send successfully.');

            return redirect()->route('normal.case.comment', ['id' => $id]);

        } else{

            return redirect()->route('normal.code-login-form');
        }

    }

    public function showCaseHistoryComment($id)
    {
        //
    }

}
