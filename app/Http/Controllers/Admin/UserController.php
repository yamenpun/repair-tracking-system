<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Gate;
use App\User;
use DB;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\User\AddFormValidation;
use App\Http\Requests\Admin\User\UpdateFormValidation;
use App\Http\Requests\Admin\User\UpdatePasswordFormValidation;


class UserController extends AdminBaseController {

    protected $view_path = 'admin.user';
    protected $base_route = 'admin.user';
    protected $model;

    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $data = [];
        $data['rows'] = User::select('id', 'username', 'email', 'role', 'created_at', 'updated_at', 'status')->where('role','!=', 'customer')->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];
        $data['row'] = User::find($id);

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        User::create([
            'username'    => $request->get('username'),
            'email'       => $request->get('email'),
            'password'    => bcrypt($request->get('password')),
            'role'        => $request->get('role'),
            'status'      => $request->get('status'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route);
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        $data = $this->model;

        if ($request->get('password') === '')
        {
            $data->update([
                'username'    => $request->get('username'),
                'email'       => $request->get('email'),
                'password'    => $data->password,
                'role'        => $request->get('role'),
                'status'      => $request->get('status'),
            ]);
        } else
        {
            $data->update([
                'username'    => $request->get('username'),
                'email'       => $request->get('email'),
                'password'    => bcrypt($request->get('password')),
                'role'        => $request->get('role'),
                'status'      => $request->get('status'),
            ]);
        }

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route);
    }

    public function editPassword($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.update'), compact('data'));
    }

    public function updatePassword(UpdatePasswordFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        $data = $this->model;

        $data->update([
            'password'    => bcrypt($request->get('password')),
        ]);

        AppHelper::flash('success', 'Password updated successfully.');

        return redirect()->route($this->base_route);
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route);
        }
        User::destroy($id);

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route);
    }

    public function profile()
    {
        $data = [];
        $data['rows'] = Auth::user();

        return view(parent::loadDefaultVars($this->view_path . '.profile'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = User::find($id);

        return $this->model;
    }
}
