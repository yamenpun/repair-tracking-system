<?php

namespace App\Http\Controllers\Admin;

use App\Models\Cases;
use App\Models\Items;
use App\User;
use Auth;
use Gate;
use DB;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\Cases\AddCaseFormValidation;
use App\Http\Requests\Admin\Cases\UpdateCaseFormValidation;
use App\Http\Requests\Admin\Cases\AddCaseTransferFormValidation;


class CasesController extends AdminBaseController {

    protected $view_path = 'admin.cases';
    protected $base_route = 'admin.cases';
    protected $model;
    protected $image_path;

    public function __construct()
    {
        parent::__construct();
        $this->image_path = config('global.path.frontend.image') . 'item';
    }

    public function index()
    {
        $data = [];
        $data['rows'] = DB::select(DB::raw(" 
                          SELECT c.*, i.item_type, u.fullname FROM cases AS c INNER  JOIN 
                          items AS i ON i.id = c.item_id INNER JOIN 
                          users AS u ON c.customer_id = u.id 
                          ORDER BY c.id DESC "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];

        $data['row'] = DB::select(DB::raw(" SELECT c.*, i.item_type, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id WHERE c.id = '$id' "));

        $data['case_comments'] = DB::select(DB::raw(" SELECT cm.case_comment_desc, cm.created_at, u.office_name, u.fullname, u.role FROM case_comment AS cm 
                                            INNER JOIN users AS u ON cm.comment_id = u.id 
                                            INNER JOIN cases AS c ON cm.case_id = c.id WHERE c.id = '$id' 
                                            ORDER BY cm.created_at DESC LIMIT 1 "));

        $data['cases_history'] = DB::select(DB::raw(" SELECT ch.* FROM cases_history AS ch
                                            INNER JOIN cases AS c ON c.id = ch.case_id WHERE c.id = '$id' "));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = DB::select(DB::raw(" SELECT c.*, i.item_type, i.item_description, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id ORDER BY c.id"));

        return view(parent::loadDefaultVars($this->view_path . '.casesPrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = DB::select(DB::raw(" SELECT c.*, i.item_type, i.item_description, u.fullname FROM cases AS c 
                                            INNER JOIN items AS i ON i.id = c.item_id
                                            INNER JOIN users AS u ON u.id = c.customer_id ORDER BY c.id"));

        $pdf = PDF::loadView($this->view_path . '.casesPdf', compact('data'));

        $timestamps = Carbon::now()->format('Y-m-d H:i:s');

        return $pdf->download('Cases List - ' . $timestamps . '.pdf');
    }

    public function exportAsExcel()
    {
        $cases = Cases::join('users', 'users.id', '=', 'cases.customer_id')
            ->join('items', 'items.id', '=', 'cases.item_id')
            ->select('cases.id', 'cases.case_date', 'users.fullname', 'cases.case_code', 'items.item_type', 'cases.case_name',
                'cases.item_brand', 'cases.item_model', 'cases.item_specs', 'cases.minimum_charge', 'cases.vendor_charge',
                'cases.total_charge', 'cases.remarks')
            ->orderBy('cases.id', 'ASC')
            ->get();


        $casesArray = [];

        // Define the Excel spreadsheet headers
        $casesArray[] = [
            'S.N.', 'Entry Date', 'Customer Name', 'Case Code', 'Item Type', 'Case Name',
            'Item Brand', 'Item Model', 'Item Specs', 'Minimum Charge', 'Vendor Charge', 'Total Charge', 'Remarks'
        ];

        // Convert each member of the returned collection into an array,
        // and append it to the student array.
        foreach ($cases as $case)
        {
            $casesArray[] = $case->toArray();
        }

        $timestamps = Carbon::now()->format('Y-m-d H:i:s');

        // Generate and return the spreadsheet
        Excel::create('Cases List - ' . $timestamps, function ($excel) use ($casesArray)
        {

            // Build the spreadsheet, passing in the student array
            $excel->sheet('sheet1', function ($sheet) use ($casesArray)
            {
                $sheet->fromArray($casesArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function create()
    {
        $data = [];

        $data['customers'] = User::select('id', 'fullname')->where('role', '=', 'customer')->get();

        $data['items'] = Items::select('id', 'item_type')->get();

        $data['vendors'] = User::select('office_name')->where('role', 'company')->get();

        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    public function store(AddCaseFormValidation $request)
    {
        Cases::create([
            'customer_id'         => $request->get('customer_id'),
            'case_code'           => AppHelper::getCaseCode(),
            'case_date'           => $request->get('case_date'),
            'item_id'             => $request->get('item_id'),
            'case_name'           => $request->get('case_name'),
            'problem_description' => $request->get('problem_description'),
            'item_brand'          => $request->get('item_brand'),
            'item_model'          => $request->get('item_model'),
            'item_specs'          => $request->get('item_specs'),
            'minimum_charge'      => $request->get('minimum_charge'),
            'total_charge'        => $request->get('total_charge'),
            'handled_by'          => 'iSewa Pvt. Ltd.',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'remarks'             => $request->get('remarks'),
        ]);

        $case = Cases::select('case_code')->where('case_date', $request->get('case_date'))->get();

        AppHelper::flash('success', 'Record created successfully with case code' . ' <b>' . $case['0']->case_code . '</b>');

        return redirect()->route($this->base_route . '.list');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];

        $data['customers'] = User::select('id', 'fullname')->where('role', '=', 'customer')->get();

        $data['items'] = Items::select('id', 'item_type')->get();

        $data['vendors'] = User::select('office_name')->where('role', 'company')->get();

        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateCaseFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = $this->model;

        $data->update([
            'customer_id'         => $request->get('customer_id'),
            'case_date'           => $request->get('case_date'),
            'item_id'             => $request->get('item_id'),
            'case_name'           => $request->get('case_name'),
            'problem_description' => $request->get('problem_description'),
            'item_brand'          => $request->get('item_brand'),
            'item_model'          => $request->get('item_model'),
            'item_specs'          => $request->get('item_specs'),
            'minimum_charge'      => $request->get('minimum_charge'),
            'total_charge'        => $request->get('total_charge'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'remarks'             => $request->get('remarks'),

        ]);

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route . '.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route . '.list');
        }

        Cases::destroy($id);

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route . '.list');
    }

    public function printCaseDetail($id)
    {
        $data = [];
        $data['case_details'] = Cases::join('users', 'users.id', '=', 'cases.customer_id')
            ->join('items', 'items.id', '=', 'cases.item_id')
            ->select('cases.id', 'cases.case_date', 'users.fullname',
                'cases.case_code', 'items.item_type', 'cases.case_name', 'cases.item_brand', 'cases.item_model',
                'cases.item_specs', 'cases.minimum_charge')
            ->where('cases.id', $id)
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.printDetails'), compact('data'));
    }

    public function transfer($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        $data['handled_by'] = User::select('id', 'office_name')->where('role', 'company')->get();

        return view(parent::loadDefaultVars($this->view_path . '.storeCaseTransfer'), compact('data'));
    }

    public function storeTransfer(AddCaseTransferFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        DB::table('cases')
            ->where('id', $id)
            ->update([
                'handled_by' => $request->get('handled_by'),
                'remarks'    => $request->get('remarks'),
            ]);

        AppHelper::flash('success', 'Case has been transferred successfully.');

        return redirect()->route($this->base_route . '.list');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Cases::find($id);

        return $this->model;
    }

}
