<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Models\Cases;
use App\Models\CasesHistory;
use App\User;

class DashboardController extends AdminBaseController
{
	protected $view_path = 'admin.dashboard';
    protected $base_route = 'admin.dashboard';

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        $data = [];
        $data['no_of_customers'] = User::where('role', 'customer')->count();
        $data['no_of_cases'] = Cases::count();
        $data['no_of_case_history'] = CasesHistory::count();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }
}
