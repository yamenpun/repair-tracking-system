<?php

namespace App\Http\Controllers\Admin;

use App\Models\CasesHistory;
use App\User;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;
use PDF;
use Excel;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\Customer\AddFormValidation;
use App\Http\Requests\Admin\Customer\UpdateFormValidation;


class CustomerController extends AdminBaseController {

    protected $view_path = 'admin.customer';
    protected $base_route = 'admin.customer';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = User::select('id', 'fullname', 'username', 'email', 'office_name', 'mobile_number', 'home_number', 'address', 'status')->where('role', '=', 'customer')->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];
        $data['row'] = User::find($id);
        $data['cases'] = DB::select(DB::raw("SELECT c.* FROM cases AS c INNER JOIN users AS u ON u.id = c.customer_id WHERE u.id = '$id' "));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function viewCaseHistory($id)
    {
        $data = [];
        $data['case_history'] = CasesHistory::join('cases', 'cases.id', '=', 'cases_history.case_id')
            ->join('users', 'users.id', '=', 'cases.customer_id')
            ->select('users.fullname', 'cases.case_name', 'cases_history.item_status', 'cases_history.description', 'cases_history.image_one', 'cases_history.image_two',
                'cases_history.remarks', 'cases_history.etr', 'cases_history.total_charge', 'cases_history.created_at', 'cases_history.updated_at',
                'cases.case_name', 'users.fullname')
            ->where('cases.id', $id)
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.viewCaseHistory'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = DB::select(DB::raw(" SELECT * FROM users WHERE role = 'customer' ORDER BY id DESC "));

        return view(parent::loadDefaultVars($this->view_path . '.customerPrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = DB::select(DB::raw(" SELECT * FROM users WHERE role = 'customer' ORDER BY id DESC "));

        $pdf = PDF::loadView($this->view_path . '.customerPdf', compact('data'));

        $timestamps = Carbon::now()->format('Y-m-d H:i:s');

        return $pdf->download('Customer List - ' . $timestamps . '.pdf');
    }

    public function exportAsExcel()
    {
        $customers = User::select('users.id', 'users.fullname', 'users.username', 'users.email', 'users.office_name', 'users.mobile_number',
            'users.home_number', 'users.address', 'users.created_at', 'users.updated_at')
            ->where('users.role', '=', 'customer')
            ->orderBy('users.id', 'ASC')
            ->get();


        $customersArray = [];

        // Define the Excel spreadsheet headers
        $customersArray[] = ['S.N.', 'Customer Name', 'Username', 'Email', 'Office Name', 'Mobile Number', 'Home Number', 'Address', 'Created At', 'Updated At'];

        // Convert each member of the returned collection into an array,
        // and append it to the student array.
        foreach ($customers as $customer)
        {
            $customersArray[] = $customer->toArray();
        }

        $timestamps = Carbon::now()->format('Y-m-d H:i:s');

        // Generate and return the spreadsheet
        Excel::create('Customer List - ' . $timestamps, function ($excel) use ($customersArray)
        {

            // Build the spreadsheet, passing in the student array
            $excel->sheet('sheet1', function ($sheet) use ($customersArray)
            {
                $sheet->fromArray($customersArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        User::create([
            'fullname'      => $request->get('fullname'),
            'username'      => $request->get('username'),
            'email'         => $request->get('email'),
            'password'      => bcrypt($request->get('password')),
            'office_name'   => $request->get('office_name'),
            'mobile_number' => $request->get('mobile_number'),
            'home_number'   => $request->get('home_number'),
            'address'       => $request->get('address'),
            'status'        => '1',
            'role'          => 'customer',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route('admin.cases.add');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        $data = $this->model;

        if ($request->get('password') === '')
        {
            $data->update([
                'fullname'      => $request->get('fullname'),
                'username'      => $request->get('username'),
                'email'         => $request->get('email'),
                'password'      => $data->password,
                'office_name'   => $request->get('office_name'),
                'mobile_number' => $request->get('mobile_number'),
                'home_number'   => $request->get('home_number'),
                'address'       => $request->get('address'),
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        } else
        {
            $data->update([
                'fullname'      => $request->get('fullname'),
                'username'      => $request->get('username'),
                'email'         => $request->get('email'),
                'password'      => bcrypt($request->get('password')),
                'office_name'   => $request->get('office_name'),
                'mobile_number' => $request->get('mobile_number'),
                'home_number'   => $request->get('home_number'),
                'address'       => $request->get('address'),
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route);
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route);
        }
        User::destroy($id);

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route);
    }

    public function profile()
    {
        $data = [];
        $data['rows'] = Auth::user();

        return view(parent::loadDefaultVars($this->view_path . '.profile'), compact('data'));
    }

    public function printCustomerDetail($id)
    {
        $data = [];
        $data['customer_details'] = User::select('fullname', 'username', 'password','address','office_name', 'mobile_number')->where('id', $id)->get();

        return view(parent::loadDefaultVars($this->view_path . '.customerPrintDetails'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = User::find($id);

        return $this->model;
    }
}
