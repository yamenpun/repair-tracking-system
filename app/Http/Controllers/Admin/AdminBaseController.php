<?php
namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AppBaseController;
use View;
use DB;

class AdminBaseController extends AppBaseController
{
    protected $user;
    protected $notifications_count;
    protected $notifications_details;

    public function __construct()
    {
        parent::__construct();

        /*$this->notifications_count = DB::select(DB::raw(" SELECT COUNT(comment_id) as notifications FROM case_comment WHERE STATUS= '0' GROUP BY 'comment_id' " ));

        $this->notifications_details = DB::select(DB::raw(" SELECT ca.id, COUNT(c.id) AS no_of_comments, u.fullname FROM case_comment AS c 
                                        INNER JOIN users AS u ON c.comment_id = u.id 
                                        INNER JOIN cases AS ca ON ca.id = c.case_id 
                                        WHERE u.role = 'customer' AND c.status = '0'
                                        GROUP BY u.id "));*/
    }

    protected function loadDefaultVars($view_path)
    {
        View::composer($view_path, function ($view) use ($view_path) {

            $view->with('view_path', parent::makeViewFolderPath($view_path));
            $view->with('base_route', $this->base_route);
            $view->with('trans_path', $this->makeViewFolderPath($view_path));
            $view->with('notifications_count', $this->notifications_count);
            $view->with('notifications_details', $this->notifications_details);

        });

        return $view_path;
    }

    protected function getArrayByKey(Collection $data, $key)
    {
        $tmp = [];
        foreach ($data as $item) {
            $tmp[] = $item->$key;
        }

        return $tmp;
    }

    
}