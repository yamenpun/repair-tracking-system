<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 255)->index();
            $table->dateTime('case_history_date');
            $table->unsignedInteger('case_id');
            $table->foreign('case_id')->references('id')->on('cases')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('item_status', ['withdrawal', 'received', 'inspected', 'wip', 'repaired', 'contact_office', 'completed', 'delivered'])->index();
            $table->string('description', 255)->nullable();
            $table->text('image_one')->nullable();
            $table->text('image_two')->nullable();
            $table->string('remarks')->nullable();
            $table->string('etr')->nullable();
            $table->double('charge', 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cases_history');
    }
}
