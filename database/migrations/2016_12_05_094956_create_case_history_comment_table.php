<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseHistoryCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_history_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('case_history_id');
            $table->foreign('case_history_id')->references('id')->on('cases_history')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger('comment_id');
            $table->foreign('comment_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->text('case_history_comment_desc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('case_history_comment');
    }
}
