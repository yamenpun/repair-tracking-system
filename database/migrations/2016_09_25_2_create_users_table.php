<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname', 50)->nullable();
            $table->string('username', 30)->unique();
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('office_name')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('home_number')->nullable();
            $table->string('address', 100)->nullable();
            $table->enum('role', [ 'admin', 'technician', 'customer', 'company' ])->index();
            $table->rememberToken();
            $table->boolean('status')->default(1);
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
