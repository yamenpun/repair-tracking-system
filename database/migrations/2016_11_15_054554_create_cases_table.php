<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('case_date');
            $table->string('case_code', 255)->unique();
            $table->unsignedInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger('item_id');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade')->onUpdate('cascade');
            $table->string('case_name', 100);
            $table->text('problem_description')->nullable();
            $table->string('item_brand', 100);
            $table->string('handled_by', 255);
            $table->string('item_model', 100)->nullable();
            $table->string('item_specs', 100)->nullable();
            $table->double('minimum_charge', 2)->nullable();
            $table->double('vendor_charge', 2)->nullable();
            $table->double('total_charge', 2)->nullable();
            $table->string('remarks', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cases');
    }
}
