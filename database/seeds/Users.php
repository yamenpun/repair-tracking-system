<?php

use Illuminate\Database\Seeder;

class Users extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username'   => 'admin',
            'email'      => 'admin@gmail.com',
            'password'   => bcrypt('admin'),
            'role'       => 'admin',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'username'   => 'technician',
            'email'      => 'technician@gmail.com',
            'password'   => bcrypt('technician'),
            'role'       => 'technician',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'office_name'   => 'iSewa Pvt. LTd',
            'fullname'      => 'Arjun Adhikari',
            'username'      => 'arjun',
            'email'         => 'arjun@gmail.com',
            'password'      => bcrypt('arjun'),
            'role'          => 'company',
            'mobile_number' => '9860453443',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'office_name'   => 'Digicom Pvt. LTd',
            'fullname'      => 'Binay Adhikari',
            'username'      => 'binay',
            'email'         => 'binay@gmail.com',
            'password'      => bcrypt('binay'),
            'role'          => 'company',
            'mobile_number' => '9860123443',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'office_name'   => 'TechnoSoft Pvt. LTd',
            'fullname'      => 'Ram Adhikari',
            'username'      => 'ram',
            'email'         => 'ram@gmail.com',
            'password'      => bcrypt('ram'),
            'role'          => 'company',
            'mobile_number' => '9860723443',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'fullname'      => 'Dinesh Budha',
            'username'      => 'dinesh',
            'mobile_number' => '9860773443',
            'password'      => bcrypt('dinesh'),
            'role'          => 'customer',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'fullname'      => 'Yamendra Pun',
            'username'      => 'yamen',
            'mobile_number' => '9860772512',
            'password'      => bcrypt('yamen'),
            'role'          => 'customer',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        ]);


    }
}
