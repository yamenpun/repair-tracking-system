@extends('technician.layouts.master')

@section('title')
    Add Items
@endsection

@section('page_specific_style')

    <link href="{{ asset('assets/admin/css/datepicker.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/admin/css/chosen.css') }}" rel="stylesheet"/>

@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('technician.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Items List</a>
                </li>

                <li class="active">Add Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Add Items Info
                    </small>

                    <div class="btn-group pull-right">
                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>
                    </div>

                </h1>

            </div>

            <div class="row-fluid">

                <div class="col-sm-12">

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                    {!! Form::open([
                    'route'     => $base_route.'.store',
                    'method'    => 'post',
                    'class'     => 'form-horizontal',
                    'role'      => "form",
                    'enctype'   => "multipart/form-data"
                    ]) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="item_name">Items Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('item_name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'item_name',
                               "placeholder"                        => "Items Name",
                               "class"                              => "col-xs-10 col-sm-12",
                               'required'                           => 'required',
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'item_name') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="item_description">Items Description</label>

                        <div class="col-sm-6 no-padding-right">

                            {!! Form::textarea('item_description', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'item_description',
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'item_description') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="item_order">Items Order <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6 no-padding-right">

                            {!! Form::text('item_order', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'item_order',
                               "class"                              => "col-xs-10 col-sm-12",
                               'required'                           => 'required',
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'item_order') !!}

                        </div>

                    </div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-4 col-md-8">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Save
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection
@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            $('.date-picker').datepicker({format: 'yyyy-mm-dd', autoclose:true}).next().on(ace.click_event, function(){
                $(this).prev().focus();
            });
        });

        $(".chosen-select").chosen();
        $('#chosen-multiple-style').on('click', function(e){
            var target = $(e.target).find('input[type=radio]');
            var which = parseInt(target.val());
            if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
            else $('#form-field-select-4').removeClass('tag-input-style');
        });

    </script>

@endsection