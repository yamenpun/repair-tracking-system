<!DOCTYPE html>

<html moznomarginboxes mozdisallowselectionprint>

    <head>

        <title>Print Items List</title>

        <style>

            @page { size: auto;  margin: 0mm; }
            table {
                border-collapse: collapse;
                width: 100%;
                padding-left:10px;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 12px;
            }
            #printpagebutton{
                background-color: #008CBA; /* Green */
                border: none;
                color: white;
                padding: 5px 8px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 12px;
            }

        </style>

    </head>

    <body>

        <div style="width: 900px; margin: 0 auto;">

            <div style="overflow-x:auto;">

                <h2 style="text-align: center">Items List &nbsp;<input id="printpagebutton" type="button" value="Print" onclick="printpage()"/></h2>

                <table>

                    <tr>
                        <th style="width: 10px;">S.N.</th>

                        <th style="width: 100px;">Item Name</th>

                        <th style="width: 200px;">Items Description</th>

                        <th style="width: 100px;">Items Order</th>

                        <th style="width: 80px;">Created At</th>

                        <th style="width: 80px;">Updated At</th>

                    </tr>

                    <?php $i = 0 ?>

                    @foreach($data['rows'] as $row)

                        <?php $i++ ?>

                        <tr>

                            <td>{{ $i }}</td>

                            <td>{{ $row->item_name }}</td>

                            <td>{{ $row->item_description }}</td>

                            <td>{{ $row->item_order }}</td>

                            <td>{{ $row->created_at }}</td>

                            <td>{{ $row->updated_at }}</td>

                        </tr>

                    @endforeach

                </table>

            </div>

        </div>

        <script type="text/javascript">

            function printpage() {

                //Get the print button and put it into a variable
                var printButton = document.getElementById("printpagebutton");

                //Set the print button visibility to 'hidden'
                printButton.style.visibility = 'hidden';

                //Print the page content
                window.print()

                //Set the print button to 'visible' again
                //[Delete this line if you want it to stay hidden after printing]
                printButton.style.visibility = 'visible';
            }

        </script>

    </body>

</html>
