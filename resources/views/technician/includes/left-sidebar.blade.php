<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('technician/dashboard')?'class="active"':'' !!}>

            <a href="{{ route('technician.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

        </li>

        <li {!! Request::is('technician/customer*')?'class="active"':'' !!}>

            <a href="{{ route('technician.customer') }}">
                <i class="icon-group"></i>
                <span class="menu-text"> Customer </span>
            </a>

        </li>

        <li {!! Request::is('technician/cases/list*') || Request::is('technician/cases/view*')
              || Request::is('technician/cases/add*') || Request::is('technician/cases/edit*')
              || Request::is('technician/cases/comment*') || Request::is('technician/case/comment/history*')
              || Request::is('technician/cases/history/add*') || Request::is('technician/cases/history/edit*')
              || Request::is('technician/cases/history/view*') ?'class="active"':'' !!}>

            <a href="{{ route('technician.cases.list') }}">
                <i class="icon-gears"></i>
                <span class="menu-text"> Cases </span>
            </a>

        </li>

        <li {!! Request::is('technician/company/list*') || Request::is('technician/company/view*') || Request::is('technician/company/add*') || Request::is('technician/company/edit*') ?'class="active"':'' !!}>

            <a href="{{ route('technician.company.list') }}">
                <i class="icon-building"></i>
                <span class="menu-text"> Vendor </span>
            </a>

        </li>

        <li {!! Request::is('technician/items/list*') || Request::is('technician/items/view*') || Request::is('technician/items/add*') || Request::is('technician/items/edit*') ?'class="active"':'' !!}>

            <a href="{{ route('technician.items.list') }}">
                <i class="icon-plus-sign"></i>
                <span class="menu-text"> Items </span>
            </a>

        </li>

        <li {!! Request::is('technician/user/profile*') || Request::is('technician/user/edit*')?'class="active"':'' !!}>

            <a href="{{ route('technician.user.profile') }}">
                <i class="icon-user"></i>
                <span class="menu-text"> User Profile</span>
            </a>

        </li>

        <li>

            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>

        </li>

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">

        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }

    </script>

</div>

