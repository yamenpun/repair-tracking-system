<!DOCTYPE html>

<html>

    <head>

        <title>Convert To PDF</title>

        <style>

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 12px;
            }

        </style>

    </head>

    <body>

        <div style="overflow-x:auto;">

            <h2 style="text-align: center">Customers List</h2>

            <table>

                <tr>

                    <th style="width: 10px;">S.N.</th>

                    <th style="width: 60px;">Customer Name</th>

                    <th style="width: 40px;">Username</th>

                    <th style="width: 40px;">Email</th>

                    <th style="width: 40px;">Office Name</th>

                    <th style="width: 40px;">Mobile No</th>

                    <th style="width: 40px;">Home No</th>

                    <th style="width: 100px;">Address</th>

                </tr>

                <?php $i = 0 ?>

                @foreach($data['rows'] as $row)

                    <?php $i ++ ?>

                    <tr>

                        <td>{{ $i }}</td>

                        <td>{{ $row->fullname }}</td>

                        <td>{{ $row->username }}</td>

                        <td>
                            @if($row->email)
                                {{ $row->email }}
                            @else
                                N/A
                            @endif
                        </td>

                        <td>
                            @if($row->office_name)
                                {{ $row->office_name }}
                            @else
                                N/A
                            @endif
                        </td>

                        <td>{{ $row->mobile_number }}</td>

                        <td>
                            @if($row->home_number)
                                {{ $row->home_number }}
                            @else
                                N/A
                            @endif
                        </td>

                        <td>
                            @if($row->address)
                                {{ $row->address }}
                            @else
                                N/A
                            @endif
                        </td>

                    </tr>

                @endforeach

            </table>

        </div>

    </body>

</html>
