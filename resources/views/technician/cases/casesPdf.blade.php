<!DOCTYPE html>

<html>

<head>

    <title>Convert To PDF</title>

    <style>

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
            font-size: 12px;
        }

    </style>

</head>

<body>

<div style="overflow-x:auto;">

    <h2 style="text-align: center">Case List</h2>

    <table>

        <tr>

            <th style="width: 10px;">S.N.</th>

            <th style="width: 70px;">Entry Date</th>

            <th style="width: 70px;">Customer Name</th>

            <th style="width: 50px;">Case Code</th>

            <th style="width: 50px;">Item Name</th>

            <th style="width: 50px;">Case Name</th>

            <th style="width: 100px;">Item Details</th>

            <th style="width: 50px;">Charge</th>

        </tr>

        <?php $i = 0 ?>

        @foreach($data['rows'] as $row)

            <?php $i ++ ?>

            <tr>

                <td>{{ $i }}</td>

                <td>{{ $row->case_date }}</td>

                <td>{{ $row->fullname }}</td>

                <td>{{ $row->case_code }}</td>

                <td>{{ $row->item_name }}</td>

                <td>{{ $row->case_name }}</td>

                <td>
                    Brand: {{ $row->item_brand }}
                    @if($row->item_model)
                        <br/>Model: {{ $row->item_model }}
                    @endif
                    @if($row->item_specs)
                        <br/>Specs:  {{ $row->item_specs }}
                    @endif

                </td>

                <td>@if($row->minimum_charge)
                        Rs. {{ $row->minimum_charge }}
                    @else
                        N/A
                    @endif
                </td>

            </tr>

        @endforeach

    </table>

</div>

</body>

</html>
