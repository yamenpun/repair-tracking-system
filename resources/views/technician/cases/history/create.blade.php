@extends('technician.layouts.master')

@section('title')
    Add Case History
@endsection

@section('page_specific_style')

    <link href="{{ asset('assets/admin/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>

@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('technician.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Case History List</a>
                </li>

                <li class="active">Add Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Add Case History Info
                    </small>

                    <div class="btn-group pull-right">
                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>
                    </div>

                </h1>

            </div>

            <div class="row-fluid">

                <div class="col-sm-12">

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                    {!! Form::open([
                    'route'     => $base_route.'.history.store',
                    'method'    => 'post',
                    'class'     => 'form-horizontal',
                    'role'      => "form",
                    'enctype'   => "multipart/form-data"
                    ]) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="customer_name">Customer Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <select name="customer_name" id="customer_name" class="col-xs-10 col-sm-12">
                                @foreach($data['cases'] as $case)
                                    <option value="{{ $case->fullname }}" {!! $case->fullname ?'selected=selected':'' !!}>{{ $case->fullname }}</option>
                                @endforeach
                            </select>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="case_id">Case Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <select name="case_id" id="case_id" class="col-xs-10 col-sm-12">
                                @foreach($data['cases'] as $case)
                                    <option value="{{ $case->id }}" {!! $case->id ?'selected=selected':'' !!}>{{ $case->case_name }}</option>
                                @endforeach
                            </select>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="case_history_date">Entry Date <span
                                    style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <div id="datetimepicker2" class="input-append">
                                <input name="case_history_date"
                                       data-format="yyyy/MM/dd HH:mm:ss PP" type="text" required>
                                <span class="add-on"><i data-time-icon="icon-time"
                                                        data-date-icon="icon-calendar"></i></span>
                            </div>

                            {!! AppHelper::getValidationErrorMsg($errors, 'case_history_date') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="item_status">Item Status <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6 no-padding-right">

                            <select name="item_status" id="item_status" class="col-xs-10 col-sm-12">
                                @foreach(config('global.item_status') as $key => $item_status)
                                    <option value="{{ $key }}" {!! $item_status?'selected=selected':'' !!}>{{ $item_status }}</option>
                                @endforeach
                            </select>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="image_one">Image One</label>

                        <div class="col-sm-6 no-padding-right">

                            {!! Form::file('image_one', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'image_one',
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'image_one') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="image_two">Image Two</label>

                        <div class="col-sm-6 no-padding-right">

                            {!! Form::file('image_two', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'image_two',
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'image_two') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="description">Description <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6 no-padding-right">

                            {!! Form::textarea('description', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'description',
                               "class"                              => "col-xs-10 col-sm-12",
                               "required"                           => "required",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'description') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="etr">Estimated Time </label>

                        <div class="col-sm-6 no-padding-right">

                            {!! Form::text('etr', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'etr',
                               "placeholder"                        => "Estimated Time",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'etr') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="total_charge">Total Cost </label>

                        <div class="col-sm-6 no-padding-right">

                            {!! Form::text('total_charge', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'total_charge',
                               "placeholder"                        => "Total Cost",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'total_charge') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="remarks">Remarks </label>

                        <div class="col-sm-6 no-padding-right">

                            {!! Form::text('remarks', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'remarks',
                               "placeholder"                        => "Remarks",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'remarks') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-4 col-md-8">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Save
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection
@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker2').datetimepicker({
                language: 'en',
                pick12HourFormat: true
            });
        });
    </script>

@endsection