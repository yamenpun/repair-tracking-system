@extends('technician.layouts.master')

@section('title')
    Add Vendor
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('technician.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Vendor List</a>
                </li>

                <li class="active">Add Form</li>

            </ul><!-- .breadcrumb -->

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Add Vendor Info
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div><!-- /.page-header -->

            <div class="row">

                <div class="col-sm-12">
                    <!-- PAGE CONTENT BEGINS -->

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                    {!! Form::open([
                    'route' => $base_route.'.store',
                    'method' => 'post',
                    'class' => 'form-horizontal',
                    'role' => "form",
                    'enctype' => "multipart/form-data"
                    ]) !!}

                    @if (count($errors) > 0)

                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>

                    @endif

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="office_name">Vendor Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('office_name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'office_name',
                               "placeholder"                        => "Company",
                               "class"                              => "col-xs-10 col-sm-12",
                               "required"                           => "required",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'office_name') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="fullname">Contact Person <span
                                    style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('fullname', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'fullname',
                               "placeholder"                        => "Contact Person",
                               "class"                              => "col-xs-10 col-sm-12",
                               "required"                           => "required",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'fullname') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="username">Username <span
                                    style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('username', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'username',
                               "placeholder"                        => "Username",
                               "class"                              => "col-xs-10 col-sm-12",
                               "required"                           => "required",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'username') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="password">Password <span
                                    style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('password', null, [
                              "kl_virtual_keyboard_secure_input"   => "on",
                              'id'                                 => 'password',
                              "placeholder"                        => "Password",
                              "class"                              => "col-xs-10 col-sm-12",
                              "required"                           => "required",
                           ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'password') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="mobile_number">Mobile No <span
                                    style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('mobile_number', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'mobile_number',
                               "placeholder"                        => "Mobile Number",
                               "class"                              => "col-xs-10 col-sm-12",
                               "required"                           => "required",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'mobile_number') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="home_number">Phone No</label>

                        <div class="col-sm-6">

                            {!! Form::text('home_number', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'home_number',
                               "placeholder"                        => "Phone Number",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'home_number') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="email">Email </label>

                        <div class="col-sm-6">

                            {!! Form::email('email', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'email',
                               "placeholder"                        => "Email",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'email') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="address">Location </label>

                        <div class="col-sm-6">

                            {!! Form::text('address', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'address',
                               "placeholder"                        => "Location",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'address') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-3 col-md-9">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Save
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

    <script type="text/javascript">

        $('#fullname').on('change', function (e) {
            console.log(e);

            var fullname = e.target.value;

            $.ajax({
                url:'/ajax-fullname?fullname=' + fullname,
                success: function(data) {
                    $('#username').val(data);
                    $('#password').val(data);
                }
            });

        });


    </script>

@endsection