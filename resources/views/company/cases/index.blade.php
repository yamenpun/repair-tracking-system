@extends('company.layouts.master')

@section('title')
    Case List
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('company.dashboard') }}">Home</a>
                </li>

                <li class="active">Case List</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Case List
                    </small>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover">

                                        <thead>
                                        <tr>
                                            <th style="width: 10px;" class="center">
                                                <label>
                                                    <input type="checkbox" class="ace"/>
                                                    <span class="lbl"></span>
                                                </label>
                                            </th>

                                            <th style="width: 40px;">Case Code</th>

                                            <th style="width: 100px;">Case Name</th>

                                            <th style="width: 80px;">Item Type</th>

                                            <th style="width: 100px;">Handled By</th>

                                            <th style="width: 100px;">Entry Date</th>

                                            <th style="width: 40px;"></th>

                                        </tr>

                                        </thead>

                                        <tbody>
                                        @foreach($data['rows'] as $row)

                                            <tr>

                                                <td class="center">
                                                    <label>
                                                        <input type="checkbox" class="ace"/>
                                                        <span class="lbl"></span>
                                                    </label>
                                                </td>

                                                <td>{{ $row->case_code }}</td>

                                                <td>{{ $row->case_name }}</td>

                                                <td>{{ $row->item_type }}</td>

                                                <td>{{ $row->handled_by }}</td>

                                                <td>{{ $row->case_date }}</td>

                                                <td>
                                                    <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

                                                        <a class="blue"
                                                           href="{{ route($base_route.'.view', ['id' => $row->id]) }}">
                                                            <i class="icon-zoom-in bigger-130" title="View Case"></i>
                                                        </a>

                                                    </div>

                                                    <div class="visible-xs visible-sm hidden-md hidden-lg">

                                                        <div class="inline position-relative">

                                                            <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                                    data-toggle="dropdown">
                                                                <i class="icon-caret-down icon-only bigger-120"></i>
                                                            </button>

                                                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">

                                                                <li>
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip"
                                                                       title="View">
                                                                    <span class="blue">
                                                                        <i class="icon-zoom-in bigger-120"></i>
                                                                    </span>
                                                                    </a>
                                                                </li>

                                                            </ul>

                                                        </div>

                                                    </div>

                                                </td>

                                            </tr>

                                        @endforeach

                                        </tbody>

                                    </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- page specific plugin scripts -->

    <script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/jquery.dataTables.bootstrap.js') }}"></script>

    <script type="text/javascript">

        jQuery(function ($) {
            var oTable1 = $('#sample-table-2').dataTable({
                "aoColumns": [
                    {"bSortable": false},
                    null, null, null, null, null,
                    {"bSortable": false}
                ]
            });


            $('table th input:checkbox').on('click', function () {
                var that = this;
                $(this).closest('table').find('tr > td:first-child input:checkbox')
                        .each(function () {
                            this.checked = that.checked;
                            $(this).closest('tr').toggleClass('selected');
                        });

            });


            $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
            function tooltip_placement(context, source) {
                var $source = $(source);
                var $parent = $source.closest('table')
                var off1 = $parent.offset();
                var w1 = $parent.width();

                var off2 = $source.offset();
                var w2 = $source.width();

                if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) return 'right';
                return 'left';
            }
        })
    </script>

@endsection
