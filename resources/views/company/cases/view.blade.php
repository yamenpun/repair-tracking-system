@extends('company.layouts.master')

@section('title')
    View Case
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('company.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Case List</a>
                </li>

                <li class="active">View Case</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ $data['row'][0]->item_type }} Case Name : {{ $data['row']['0']->case_name }}
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="page-content">

                <div class="row">

                    <div class="col-xs-12">

                        <table class="table table-striped table-bordered table-hover "
                               id="" aria-describedby="sample-table-2_info">

                            <thead>

                            <tr>
                                <th style="width: 100px;">Entry Date</th>

                                <th style="width: 80px;">Case Code</th>

                                <th style="width: 150px;">Item Detail</th>

                                <th style="width: 100px;">Problem Description</th>

                                <th style="width: 80px;">Vendor Charge</th>

                                <th style="width: 80px;">Remarks</th>

                                <th style="width: 30px;">Comment</th>

                            </tr>

                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">

                            <tr class="odd">

                                <td>{{ $data['row']['0']->case_date }} </td>
                                <td>{{ $data['row']['0']->case_code }} </td>
                                <td>
                                    {{ $data['row']['0']->item_brand }}
                                    @if($data['row']['0']->item_model)
                                        , {{ $data['row']['0']->item_model }}
                                    @endif
                                    @if($data['row']['0']->item_specs)
                                        , {{ $data['row']['0']->item_specs }}
                                    @endif
                                </td>
                                <td>
                                    @if($data['row']['0']->problem_description)
                                        {{ $data['row']['0']->problem_description }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>
                                    @if($data['row']['0']->vendor_charge)
                                        Rs. {{ $data['row']['0']->vendor_charge }}
                                        &nbsp;
                                        <a href="{{ route('company.cases.charge.edit', ['id' => $data['row'][0]->id]) }}">
                                            <button class="btn btn-xs btn-success" title="Update Charge">
                                                <i class="icon-edit bigger-120"></i>
                                            </button>
                                        </a>
                                    @else
                                        N/A
                                        &nbsp;
                                        <a href="{{ route('company.cases.charge.add', ['id' => $data['row'][0]->id]) }}">
                                            <button class="btn btn-xs btn-primary" title="Add Charge">
                                                <i class="icon-plus-sign-alt bigger-120"></i>
                                            </button>
                                        </a>
                                    @endif

                                </td>
                                <td>
                                    @if($data['row']['0']->remarks)
                                       {{ $data['row']['0']->remarks }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>
                                    <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

                                        <a class="blue"
                                           href="{{ route('company.cases.comment.list', ['id' => $data['row']['0']->id]) }}">
                                            <i class="icon-comment-alt bigger-130" title="View Case Comment"></i>
                                        </a>

                                    </div>

                                    <div class="visible-xs visible-sm hidden-md hidden-lg">

                                        <div class="inline position-relative">

                                            <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <i class="icon-caret-down icon-only bigger-120"></i>
                                            </button>

                                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">

                                                <li>
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" title="View">
                                                                <span class="blue">
                                                                    <i class="icon-zoom-in bigger-120"></i>
                                                                </span>
                                                    </a>
                                                </li>

                                            </ul>

                                        </div>

                                    </div>
                                </td>

                            </tr>

                            </tbody>

                        </table>

                    </div>

                    <div class="col-xs-12">
                        <div class="widget-box ">
                            <div class="widget-header">
                                <h4 class="lighter smaller">
                                    <i class="icon-comment blue"></i>
                                    Top Comments
                                </h4>
                            </div>

                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                    <div class="slimScrollDiv"
                                         style="position: relative; overflow: hidden; width: auto; height: auto;">
                                        <div class="dialogs" style="overflow: hidden; width: auto; height: auto;">
                                            @if($data['case_comments'] != null)
                                                @foreach($data['case_comments'] as $comment)

                                                    <div class="itemdiv dialogdiv">

                                                        <div class="user">
                                                            @if($comment->role == 'customer')
                                                                <img alt="Alexa's Avatar"
                                                                     src="{{ asset('assets/admin/avatars/avatar1.png') }}">
                                                            @elseif($comment->role == 'admin' || $comment->role == 'technician')
                                                                <img alt="Alexa's Avatar"
                                                                     src="{{ asset('assets/admin/avatars/avatar5.png') }}">
                                                            @elseif($comment->role == 'company')
                                                                <img alt="Alexa's Avatar"
                                                                     src="{{ asset('assets/admin/avatars/avatar.png') }}">
                                                            @endif
                                                        </div>

                                                        <div class="body">
                                                            <div class="time">
                                                                <i class="icon-time"></i>
                                                                <span class="green">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() }}</span>
                                                            </div>

                                                            <div class="name">
                                                                @if($comment->role == 'customer')
                                                                    <a href="#">{{ $comment->fullname }}</a>
                                                                @elseif($comment->role == 'admin' || $comment->role == 'technician')
                                                                    <a href="#">Admin</a>
                                                                @elseif($comment->role == 'company')
                                                                    <a href="#">You</a>
                                                                @endif
                                                            </div>
                                                            <div class="text">{{ $comment->case_comment_desc }}</div>
                                                        </div>

                                                    </div>

                                                @endforeach
                                            @else
                                                <div>No comments to show.</div><br>
                                            @endif

                                        </div>
                                        <div class="slimScrollBar ui-draggable"
                                             style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 74px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 225.564px;"></div>
                                        <div class="slimScrollRail"
                                             style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br><br>

                    </div>

                    <div class="page-header">

                        <h1>

                            <small>
                                <i class="icon-double-angle-right"></i>
                                Case History Details
                            </small>

                            <div class="btn-group">

                                <a href="{{ route($base_route.'.history.add', ['id' => $data['row']['0']->id]) }}"
                                   class="btn btn-primary btn-sm">
                                    <i class="icon-plus-sign bigger-110"></i>
                                    Add Case History
                                </a>

                            </div>

                        </h1>

                    </div>

                    <div class="col-xs-12">

                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">

                            <thead>
                            <tr>

                                <th style="width: 100px;">Entry Date</th>

                                <th style="width: 100px;">Item Status</th>

                                <th style="width: 150px;">Description</th>

                                <th style="width: 100px;">Image</th>

                                <th style="width: 40px;">Time</th>

                                <th style="width: 40px;">Charge</th>

                                <th style="width: 40px;">Remarks</th>

                                <th style="width: 30px;">Comment</th>

                            </tr>

                            </thead>

                            <tbody>
                            @if($data['cases_history'] != null)

                                @foreach($data['cases_history'] as $row)
                                    <tr>
                                        <td>{{ $row->case_history_date }}</td>

                                        <td>{{  AppHelper::getItemStatusDetail($row->item_status) }}</td>

                                        <td>{{ $row->description }}</td>

                                        <td>
                                            @if($row->image_one)
                                                <img src="{{ asset(config('global.url.frontend.image').'item/'.$row->image_one) }}"
                                                     alt=""
                                                     style="max-width: 200px; max-height:50px">
                                            @else
                                                <p>No Image Uploaded.</p>
                                            @endif
                                        </td>

                                        <td>
                                            @if($row->etr)
                                                {{ $row->etr }}
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                        <td>
                                            @if($row->charge == '0')
                                                N/A
                                            @else
                                                {{ $row->charge }}
                                            @endif
                                        </td>

                                        <td>
                                            @if($row->remarks)
                                                {{ $row->remarks }}
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                        <td>
                                            <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

                                                <a class="blue"
                                                   href="{{ route($base_route.'.history.view', ['id' => $row->id]) }}">
                                                    <i class="icon-zoom-in bigger-130" title="View Case History"></i>
                                                </a>

                                                <a class="green"
                                                   href="{{ route($base_route.'.history.edit', ['id' => $row->id]) }}">
                                                    <i class="icon-pencil bigger-130" title="Update Case History"></i>
                                                </a>

                                                {{--<a class="red deleteConfirm"
                                                   href="{{ route($base_route.'.history.delete', ['id' => $row->id]) }}">
                                                    <i class="icon-trash bigger-130" title="Delete Case History"></i>
                                                </a>--}}

                                                <a class="blue"
                                                   href="{{ route('company.cases.comment-history.list', ['id' => $row->id]) }}">
                                                    <i class="icon-comment-alt bigger-130" title="View Case History Comment"></i>
                                                </a>

                                            </div>

                                            <div class="visible-xs visible-sm hidden-md hidden-lg">

                                                <div class="inline position-relative">

                                                    <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        <i class="icon-caret-down icon-only bigger-120"></i>
                                                    </button>

                                                    <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">

                                                        <li>
                                                            <a href="#" class="tooltip-info" data-rel="tooltip"
                                                               title="View">
                                                                        <span class="blue">
                                                                            <i class="icon-zoom-in bigger-120"></i>
                                                                        </span>
                                                            </a>
                                                        </li>

                                                        {{--<li>
                                                            <a href="#" class="tooltip-success" data-rel="tooltip"
                                                               title="Edit">
                                                                            <span class="green">
                                                                                <i class="icon-edit bigger-120"></i>
                                                                            </span>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#" class="tooltip-error" data-rel="tooltip"
                                                               title="Delete">
                                                                            <span class="red">
                                                                                <i class="icon-trash bigger-120"></i>
                                                                            </span>
                                                            </a>
                                                        </li>--}}

                                                        <li>
                                                            <a href="#" class="tooltip-error" data-rel="tooltip"
                                                               title="Comment">
                                                                <span class="red">
                                                                    <i class="icon-trash bigger-120"></i>
                                                                </span>
                                                            </a>
                                                        </li>

                                                    </ul>

                                                </div>

                                            </div>
                                        </td>

                                    </tr>

                                @endforeach

                            @else
                                <tr>
                                    <td colspan="8">There is no history recorded for this case.</td>
                                </tr>
                            @endif
                            </tbody>

                        </table>
                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
