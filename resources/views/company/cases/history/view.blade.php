@extends('company.layouts.master')

@section('title')
    View Case History
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('company.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Case List</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.view', ['id' => $data['row']['0']->case_id]) }}">View Case</a>
                </li>

                <li class="active">View Case History</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Case History Detail Info
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12" style="padding-left: 50px">

                    <div class="row">

                        <div class="col-xs-8">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table class="table table-striped table-bordered table-hover "
                                       id="" aria-describedby="sample-table-2_info">

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                                    <tr class="even">

                                        <td>Entry Date</td>

                                        <td>{{ $data['row']['0']->case_history_date }}</td>

                                    </tr>

                                    <tr class="odd">

                                        <td>Customer Name</td>
                                        <td>{{ $data['row']['0']->fullname }}</td>

                                    </tr>

                                    <tr class="even">

                                        <td>Case Code</td>

                                        <td>{{ $data['row']['0']->case_code }}</td>

                                    </tr>

                                    <tr class="even">

                                        <td>Case Name</td>
                                        <td>{{ $data['row']['0']->case_name }} </td>

                                    </tr>

                                    <tr class="odd">

                                        <td>Item Details</td>
                                        <td>{{ $data['row']['0']->item_type }}
                                            , {{ $data['row']['0']->item_brand }}
                                            @if($data['row']['0']->item_model)
                                                , {{ $data['row']['0']->item_model }}
                                            @endif
                                        </td>

                                    </tr>

                                    <tr class="odd">

                                        <td>Item Status</td>
                                        <td>{{  AppHelper::getItemStatusDetail($data['row']['0']->item_status) }}</td>

                                    </tr>

                                    <tr class="even">
                                        <td>Description</td>
                                        <td>{{ $data['row']['0']->description }}</td>
                                    </tr>

                                    <tr class="odd">
                                        <td>Image One</td>
                                        <td>
                                            @if ($data['row'][0]->image_one)
                                                <img src="{{ asset('assets/images/item/'.$data['row'][0]->image_one) }}"
                                                     alt="{{ $data['row'][0]->item_status }}" class="img-responsive"
                                                     width="100">
                                            @else
                                                No Image Uploaded.
                                            @endif
                                        </td>

                                    </tr>

                                    <tr class="even">
                                        <td>Image Two</td>
                                        <td>
                                            @if ($data['row'][0]->image_two)
                                                <img src="{{ asset('assets/images/item/'.$data['row'][0]->image_two) }}"
                                                     alt="{{ $data['row'][0]->item_status }}" class="img-responsive"
                                                     width="100">
                                            @else
                                                No Image Uploaded.
                                            @endif
                                        </td>
                                    </tr>

                                    <tr class="odd">
                                        <td>Estimated Time</td>
                                        <td>
                                            @if($data['row']['0']->etr)
                                                {{ $data['row']['0']->etr }}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                    </tr>

                                    <tr class="even">
                                        <td>Total Charge</td>
                                        <td>
                                            @if($data['row']['0']->charge)
                                                Rs. {{ $data['row']['0']->charge }}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                    </tr>

                                    <tr class="odd">
                                        <td>Remarks</td>
                                        <td>
                                            @if($data['row']['0']->remarks)
                                                {{ $data['row']['0']->remarks }}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                    </tr>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
