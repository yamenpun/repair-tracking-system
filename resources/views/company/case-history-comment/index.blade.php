@extends('company.layouts.master')

@section('title')
    Case History Comment
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('company.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route('company.cases.list') }}">Case List</a>
                </li>

                <li>
                    <a href="{{ route('company.cases.view', ['id' => $data['row']['0']->id]) }}">View Cases</a>
                </li>

                <li class="active">View Case History Comment</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Case History Comment Of Case : {{ $data['row']['0']->case_name }}
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="page-content">

                <div class="row">

                    <div class="col-xs-12">

                        @if (session()->has('message'))
                            {!! session()->get('message') !!}
                        @endif

                        <table class="table table-striped table-bordered table-hover" id=""
                               aria-describedby="sample-table-2_info">

                            <thead>

                            <tr>

                                <th style="width: 80px;">Entry Date</th>

                                <th style="width: 100px;">Item Status</th>

                                <th style="width: 120px;">Description</th>

                                <th style="width: 80px;">Image</th>

                                <th style="width: 50px;">Time</th>

                                <th style="width: 50px;">Charge</th>

                                <th style="width: 50px;">Remarks</th>

                            </tr>

                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">

                            <tr class="odd">
                                <td>{{ $data['row']['0']->case_history_date }} </td>
                                <td>{{  AppHelper::getItemStatusDetail($data['row']['0']->item_status) }}</td>
                                <td>{{ $data['row']['0']->description }} </td>
                                <td>
                                    @if ($data['row'][0]->image_one)
                                        <img src="{{ asset('assets/images/item/'.$data['row'][0]->image_one) }}"
                                             alt="{{ $data['row'][0]->item_status }}" class="img-responsive"
                                             width="100">
                                    @else
                                        No Image Uploaded.
                                    @endif
                                </td>
                                <td>
                                    @if($data['row']['0']->etr)
                                        {{ $data['row']['0']->etr }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>
                                    @if($data['row']['0']->charge)
                                        Rs. {{ $data['row']['0']->charge }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>
                                    @if($data['row']['0']->remarks)
                                        {{ $data['row']['0']->remarks }}
                                    @else
                                        N/A
                                    @endif
                                </td>

                            </tr>

                            </tbody>

                        </table>

                    </div>

                    <div class="col-xs-12">
                        <div class="widget-box ">
                            <div class="widget-header">
                                <h4 class="lighter smaller">
                                    <i class="icon-comment blue"></i>
                                    Comments
                                </h4>
                            </div>

                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                    <div class="slimScrollDiv"
                                         style="position: relative; overflow: hidden; width: auto; height: auto;">
                                        <div class="dialogs" style="overflow: hidden; width: auto; height: auto;">
                                            @if($data['case_history_comments'] != null)
                                                @foreach($data['case_history_comments'] as $comment)

                                                    <div class="itemdiv dialogdiv">

                                                        <div class="user">
                                                            @if($comment->role == 'customer')
                                                                <img alt="Alexa's Avatar"
                                                                     src="{{ asset('assets/admin/avatars/avatar1.png') }}">
                                                            @elseif($comment->role == 'admin' || $comment->role == 'technician')
                                                                <img alt="Alexa's Avatar"
                                                                     src="{{ asset('assets/admin/avatars/avatar5.png') }}">
                                                            @elseif($comment->role == 'company')
                                                                <img alt="Alexa's Avatar" src="{{ asset('assets/admin/avatars/avatar.png') }}" >
                                                            @endif
                                                        </div>

                                                        <div class="body">
                                                            <div class="time">
                                                                <i class="icon-time"></i>
                                                                <span class="green">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() }}</span>
                                                            </div>

                                                            <div class="name">
                                                                @if($comment->role == 'customer')
                                                                    <a href="#">{{ $comment->fullname }}</a>
                                                                @elseif($comment->role == 'admin' || $comment->role == 'technician')
                                                                    <a href="#">Admin</a>
                                                                @elseif($comment->role == 'company')
                                                                    <a href="#">You</a>
                                                                @endif
                                                            </div>
                                                            <div class="text">{{ $comment->case_history_comment_desc }}</div>
                                                        </div>

                                                    </div>

                                                @endforeach
                                            @else
                                                <div>No comments to show.</div><br>
                                            @endif

                                        </div>
                                        <div class="slimScrollBar ui-draggable"
                                             style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 74px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 225.564px;"></div>
                                        <div class="slimScrollRail"
                                             style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>

                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    {!! Form::model($data['row'], [
                                        'route'     => ['technician.cases.comment-history.store', $data['row']['0']->id],
                                        'method'    => 'post',
                                        'class'     => 'form-horizontal',
                                        'role'      => "form",
                                        'enctype'   => "multipart/form-data"
                                        ]) !!}

                                    <div class="form-actions">
                                        <div class="input-group">
                                            <input placeholder="Type your message here ..." class="form-control"
                                                   name="case_history_comment_desc" type="text" required>
                                            {!! AppHelper::getValidationErrorMsg($errors, 'case_history_comment_desc') !!}
                                            <span class="input-group-btn">
                                                        <button class="btn btn-sm btn-info no-radius" type="submit">
                                                            <i class="icon-share-alt"></i>
                                                            Send
                                                        </button>
                                                    </span>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
