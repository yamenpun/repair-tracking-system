<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('company/dashboard')?'class="active"':'' !!}>

            <a href="{{ route('company.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

        </li>

        <li {!! Request::is('company/cases/list*') || Request::is('company/cases/view*') ||
                Request::is('company/cases/comment*') || Request::is('company/case/comment/history*') ||
                Request::is('company/cases/history/add*') || Request::is('company/cases/history/edit*') ||
                Request::is('company/cases/history/view*') || Request::is('company/cases/charge/*') ?'class="active"':'' !!}>

            <a href="{{ route('company.cases.list') }}">
                <i class="icon-gears"></i>
                <span class="menu-text"> Cases </span>
            </a>

        </li>

        <li {!! Request::is('company/user/profile*') || Request::is('company/user/edit*')?'class="active"':'' !!}>

            <a href="{{ route('company.user.profile') }}">
                <i class="icon-user"></i>
                <span class="menu-text"> User Profile</span>
            </a>

        </li>

        <li>

            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>

        </li>

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">

        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }

    </script>

</div>

