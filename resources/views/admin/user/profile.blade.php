@extends('admin.layouts.master')

@section('title')
    User Profile
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route) }}">User List</a>
                </li>

                <li class="active">User Profile</li>

            </ul><!-- .breadcrumb -->

        </div>

        <div class="page-content">

            <div class="page-header">
                <h1>
                    <small>
                        <i class="icon-double-angle-right"></i>
                        User Profile
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm ">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>
            </div><!-- /.page-header -->

            <div class="row">

                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div>

                        <div id="user-profile-1" class="user-profile row">

                            <div class="col-xs-12 col-sm-9">

                                <div class="profile-user-info profile-user-info-striped">

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Username</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="username">{{ $data['rows']->username }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Email</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="email">{{ $data['rows']->email }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">User Role</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="role">{{ $data['rows']->role }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Created At</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="created_at">{{ date('jS M, Y', strtotime($data['rows']->created_at)) }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Updated At</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="updated_at">{{ date('jS M, Y', strtotime($data['rows']->updated_at)) }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Status</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="age">
                                               @if($data['rows']->status == 1)
                                                    <button class="btn btn-minier btn-primary">Active</button>
                                                @else
                                                    <button class="btn btn-minier btn-yellow">Inactive</button>
                                                @endif
                                            </span>
                                        </div>

                                    </div>

                                </div>

                                <div class="space-6"></div>

                            </div>

                        </div>

                    </div>

                    <!-- PAGE CONTENT ENDS -->

                </div>

            </div>

        </div>

    </div>

@endsection