@extends('admin.layouts.master')

@section('title')
    Update User
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route) }}">User List</a>
                </li>

                <li class="active">Update Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Update User Info
                    </small>

                    <div class="btn-group pull-right">
                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>
                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-sm-12">
                    <!-- PAGE CONTENT BEGINS -->

                    {!! Form::model($data['row'], [
                    'route'     => [$base_route.'.update', $data['row']->id],
                    'method'    => 'post',
                    'class'     => 'form-horizontal',
                    'role'      => "form",
                    'enctype'   => "multipart/form-data"
                    ]) !!}

                    <input type="hidden" name="id" value="{{ $data['row']->id }}">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="username">Username <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('username', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'username',
                               "placeholder"                        => "Username",
                               "class"                              => "col-xs-10 col-sm-12",
                               "required"                           => "required",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'username') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="email">Email </label>

                        <div class="col-sm-6">

                            {!! Form::email('email', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'email',
                               "placeholder"                        => "Email",
                               "class"                              => "col-xs-10 col-sm-12",
                               "required"                           => "required",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'email') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="password">Password</label>

                        <div class="col-sm-6">

                            {!! Form::password('password', null, [
                              "kl_virtual_keyboard_secure_input"   => "on",
                              'id'                                 => 'password',
                              "placeholder"                        => "Password",
                              "class"                              => "col-xs-10 col-sm-12",
                              "required"                           => "required",
                           ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'password') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="password_confirmation">Confirm Password</label>

                        <div class="col-sm-6">

                            {!! Form::password('password_confirmation', null, [
                              "kl_virtual_keyboard_secure_input"   => "on",
                              'id'                                 => 'password_confirmation',
                              "class"                              => "col-xs-10 col-sm-12",
                              "required"                           => "required",
                           ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'password_confirmation') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="role">User Role</label>

                        <div class="col-sm-2">

                            <select name="role" id="role" class="col-xs-10 col-sm-12">

                                @foreach(config('global.admin-role') as $key => $role)
                                    <?php
                                    $selected = false;

                                    if ($data['row']->role == $key) {
                                        $selected = true;
                                    }
                                    if (old('role')) {
                                        if (old('role') == $key) {
                                            $selected = true;
                                        } else {
                                            $selected = false;
                                        }
                                    }
                                    ?>
                                    <option value="{{ $key }}" {!! $selected?'selected=selected':'' !!}>{{ $role }}</option>
                                @endforeach

                            </select>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="status">Status</label>

                        <div class="col-sm-6">

                            <div class="control-group">

                                <div class="radio">
                                    <label>
                                        {!! Form::radio('status', 1, true, [
                                            'class' => 'ace'
                                        ]) !!}
                                        <span class="lbl">Active</span>
                                    </label>
                                </div>

                                <div class="radio">
                                    <label>
                                        {!! Form::radio('status', 0, false, [
                                             'class' => 'ace'
                                         ]) !!}
                                        <span class="lbl">Inactive</span>
                                    </label>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-3 col-md-9">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Update
                            </button>

                            &nbsp; &nbsp; &nbsp;
                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection