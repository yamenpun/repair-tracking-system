<!DOCTYPE html>

<html moznomarginboxes mozdisallowselectionprint>

<head>

    <title>Customer Case Card</title>

    <style>

        @page {
            size: auto;
            margin: 0mm;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            padding-left: 10px;
        }

        th, td {
            text-align: left;
            padding: 8px;
            font-size: 12px;
        }

        #printpagebutton {
            background-color: green; /* Green */
            border: none;
            color: white;
            padding: 5px 8px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
        }

    </style>

</head>

<body>

<div style="width: 900px; margin: 0 auto;">

    <div style="margin: 0 50px;">

        <table border="1px" width="600px">

            <tr>
                <td>

                    <div align="center"><img src="{{ asset('assets/images/logo/laptopnepal.png') }}" alt="Laptop Nepal Logo" class="img-responsive" height="100px;" width="500px"> &nbsp;<input id="printpagebutton" type="button" value="Print" onclick="printpage()"/></div>

                    <h1 style="text-align: center; text-decoration: underline;">Customer Copy Case Card</h1>

                    <table border="0px">

                        <tr>

                            <th style="width: 300px; text-align: right; font-size: 16px;">Case Code:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->case_code }}</td>

                        </tr>

                        <tr>

                            <th style="width: 300px; text-align: right; font-size: 16px;">Customer Name:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->fullname }}</td>

                        </tr>

                        <tr>

                            <th style="width: 300px; text-align: right; font-size: 16px;">Entry Date:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->case_date }}</td>

                        </tr>

                        <tr>

                            <th style="width: 300px; text-align: right; font-size: 16px;">Item Problem:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->case_name }}</td>

                        </tr>

                        <tr>

                            <th style="width: 100px; text-align: right; font-size: 16px;">Item:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->item_type }}, {{ $data['case_details']['0']->item_brand }}, {{ $data['case_details']['0']->item_model }} </td>

                        </tr>

                        <tr>

                            <th style="width: 100px; text-align: right; font-size: 16px;">Specs:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->item_specs }}</td>

                        </tr>

                        <tr>

                            <th style="width: 100px; text-align: right; font-size: 16px;">Minimum Charge:</th>

                            <td style="font-size: 16px;">Rs. {{ $data['case_details']['0']->minimum_charge }}</td>

                        </tr>

                        <tr>

                            <th style="width: 100px; text-align: right; font-size: 16px;">Tracking Link:</th>

                            <td style="font-size: 16px;">http://laptopnepal.com/tracking</td>

                        </tr>

                    </table>

                </td>

            </tr>

        </table>

        <br>
        <br>
        <br>

        <table border="1px" width="600px">

            <tr>
                <td>

                    <div align="center"><img src="{{ asset('assets/images/logo/laptopnepal.png') }}" alt="Laptop Nepal Logo" class="img-responsive" height="100px;" width="500px"></div>

                    <h1 style="text-align: center; text-decoration: underline;">Office Copy Case Card</h1>

                    <table border="0px">

                        <tr>

                            <th style="width: 300px; text-align: right; font-size: 16px;">Case Code:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->case_code }}</td>

                        </tr>

                        <tr>

                            <th style="width: 300px; text-align: right; font-size: 16px;">Customer Name:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->fullname }}</td>

                        </tr>

                        <tr>

                            <th style="width: 300px; text-align: right; font-size: 16px;">Entry Date:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->case_date }}</td>

                        </tr>

                        <tr>

                            <th style="width: 300px; text-align: right; font-size: 16px;">Item Problem:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->case_name }}</td>

                        </tr>

                        <tr>

                            <th style="width: 100px; text-align: right; font-size: 16px;">Item:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->item_name }}, {{ $data['case_details']['0']->item_brand }}, {{ $data['case_details']['0']->item_model }} </td>

                        </tr>

                        <tr>

                            <th style="width: 100px; text-align: right; font-size: 16px;">Specs:</th>

                            <td style="font-size: 16px;">{{ $data['case_details']['0']->item_specs }}</td>

                        </tr>

                        <tr>

                            <th style="width: 100px; text-align: right; font-size: 16px;">Minimum Charge:</th>

                            <td style="font-size: 16px;">Rs. {{ $data['case_details']['0']->minimum_charge }}</td>

                        </tr>

                        <tr>

                            <th style="width: 100px; text-align: right; font-size: 16px;">Tracking Link:</th>

                            <td style="font-size: 16px;">http://laptopnepal.com/tracking</td>

                        </tr>

                    </table>

                </td>

            </tr>

        </table>

    </div>

</div>

<script type="text/javascript">

    function printpage() {

        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");

        //Set the print button visibility to 'hidden'
        printButton.style.visibility = 'hidden';

        //Print the page content
        window.print()

        //Set the print button to 'visible' again
        //[Delete this line if you want it to stay hidden after printing]
        printButton.style.visibility = 'visible';
    }

</script>

</body>

</html>
