@extends('admin.layouts.master')

@section('title')
    Update Case
@endsection

@section('page_specific_style')

    <link href="{{ asset('assets/admin/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>

@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Case List</a>
                </li>

                <li class="active">Update Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Update Case Info
                    </small>

                    <div class="btn-group pull-right">
                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>
                    </div>

                </h1>

            </div>

            <div class="row-fluid">

                <div class="col-sm-12">

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                    {!! Form::model($data['row'], [
                    'route'     => [$base_route.'.update', $data['row']->id],
                    'method'    => 'post',
                    'class'     => 'form-horizontal',
                    'role'      => "form",
                    'enctype'   => "multipart/form-data"
                    ]) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="customer_id">Customer Name <span
                                        style="color: #bf1d1d;">*</span></label>

                            <div class="col-sm-6">

                                <select name="customer_id" id="customer_id" class="col-xs-10 col-sm-12">
                                    @foreach($data['customers'] as $customer)
                                        <?php
                                        $selected = false;

                                        if ($data['row']->customer_id == $customer->id)
                                        {
                                            $selected = true;
                                        }
                                        if (old('customer_id'))
                                        {
                                            if (old('customer_id') == $customer->id)
                                            {
                                                $selected = true;
                                            } else
                                            {
                                                $selected = false;
                                            }
                                        }
                                        ?>
                                        <option value="{{ $customer->id }}" {!! $selected?'selected=selected':'' !!}>{{ $customer->fullname }}</option>
                                    @endforeach
                                </select>

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="case_date">Case Date <span
                                        style="color: #bf1d1d;">*</span></label>

                            <div class="col-sm-6">

                                <div id="datetimepicker2" class="input-append">
                                    <input name="case_date" value="{{ $data['row']->case_date }}"
                                           data-format="yyyy/MM/dd HH:mm:ss PP" type="text" required>
                                    <span class="add-on"><i data-time-icon="icon-time"
                                                            data-date-icon="icon-calendar"></i></span>
                                </div>

                                {!! AppHelper::getValidationErrorMsg($errors, 'case_date') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="item_id">Item Name <span
                                        style="color: #bf1d1d;">*</span></label>

                            <div class="col-sm-6">

                                <select name="item_id" id="item_id" class="col-xs-10 col-sm-12">
                                    @foreach($data['items'] as $item)
                                        <?php
                                        $selected = false;

                                        if ($data['row']->item_id == $item->id)
                                        {
                                            $selected = true;
                                        }
                                        if (old('item_id'))
                                        {
                                            if (old('item_id') == $item->id)
                                            {
                                                $selected = true;
                                            } else
                                            {
                                                $selected = false;
                                            }
                                        }
                                        ?>
                                        <option value="{{ $item->id }}" {!! $selected?'selected=selected':'' !!}>{{ $item->item_type }}</option>
                                    @endforeach
                                </select>

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="case_name">Case Name <span
                                        style="color: #bf1d1d;">*</span></label>

                            <div class="col-sm-6 no-padding-right">

                                {!! Form::text('case_name', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'case_name',
                                   "class"                              => "col-xs-10 col-sm-12",
                                   "required"                           => "required",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'case_name') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="item_brand">Item Brand <span
                                        style="color: #bf1d1d;">*</span></label>

                            <div class="col-sm-6 no-padding-right">

                                {!! Form::text('item_brand', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'item_brand',
                                   "class"                              => "col-xs-10 col-sm-12",
                                   "required"                           => "required",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'item_brand') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="item_model">Item Model </label>

                            <div class="col-sm-6 no-padding-right">

                                {!! Form::text('item_model', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'item_model',
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'item_model') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="item_specs">Item Specifications </label>

                            <div class="col-sm-6 no-padding-right">

                                {!! Form::textarea('item_specs', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'item_specs',
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'item_specs') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="problem_description">Problem Description</label>

                            <div class="col-sm-6 no-padding-right">

                                {!! Form::textarea('problem_description', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'problem_description',
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'problem_description') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="minimum_charge">Minimum
                                Charge </label>

                            <div class="col-sm-6 no-padding-right">

                                {!! Form::text('minimum_charge', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'minimum_charge',
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'minimum_charge') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="total_charge">Total
                                Charge </label>

                            <div class="col-sm-6 no-padding-right">

                                {!! Form::text('total_charge', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'total_charge',
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'total_charge') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="clearfix form-actions">

                            <div class="col-md-offset-4 col-md-8">

                                <button class="btn btn-primary" type="submit">
                                    <i class="icon-ok bigger-110"></i>
                                    Update
                                </button>

                                &nbsp; &nbsp; &nbsp;

                                <button class="btn btn-primary" type="reset">
                                    <i class="icon-undo bigger-110"></i>
                                    Reset
                                </button>

                            </div>

                        </div>

                        {!! Form::close() !!}

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker2').datetimepicker({
                language: 'en',
                pick12HourFormat: true
            });
        });
    </script>

@endsection