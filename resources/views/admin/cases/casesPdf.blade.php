<!DOCTYPE html>

<html>

<head>

    <title>Convert To PDF</title>

    <style>

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
            font-size: 9px;
        }

    </style>

</head>

<body>

<div style="overflow-x:auto;">

    <h2 style="text-align: center">Case List</h2>

    <table border="1">

        <tr>

            <th style="width: 10px;">S.N.</th>

            <th style="width: 80px;">Entry Date</th>

            <th style="width: 90px;">Customer Info</th>

            <th style="width: 60px;">Case Name</th>

            <th style="width: 140px;">Item Details</th>

            <th style="width: 100px;">Problem Description</th>

            <th style="width: 60px;">Total Charge</th>

            <th style="width: 50px;">Remarks</th>

        </tr>

        <?php $i = 0 ?>

        @foreach($data['rows'] as $row)

            <?php $i++ ?>

            <tr>

                <td>{{ $i }}</td>

                <td>{{ $row->case_date }}</td>

                <td>
                    Name : {{ $row->fullname }}<br/>
                    Code : {{ $row->case_code }}
                </td>

                <td>{{ $row->case_name }}</td>

                <td>
                    {{ $row->item_type }}, {{ $row->item_brand }}
                    @if($row->item_model)
                        , {{ $row->item_model }}
                    @endif
                    @if($row->item_specs)
                        , {{ $row->item_specs }}
                    @endif

                </td>

                <td>
                    @if($row->problem_description)
                        {{ $row->problem_description }}
                    @else
                        N/A
                    @endif
                </td>

                <td>@if($row->total_charge)
                        Rs. {{ $row->total_charge }}
                    @else
                        N/A
                    @endif
                </td>

                <td>@if($row->remarks)
                        {{ $row->remarks }}
                    @else
                        N/A
                    @endif
                </td>

            </tr>

        @endforeach

    </table>

</div>

</body>

</html>
