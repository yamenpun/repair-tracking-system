@extends('admin.layouts.master')

@section('title')
    View Customer
@endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route) }}">Customer List</a>
                </li>

                <li class="active">View Customer</li>
            </ul>
        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Detail Info Of Customer : {{ $data['row']->fullname }}
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="page-content">

                <div class="row">

                    <div class="col-xs-12">

                        <table class="table table-striped table-bordered table-hover" id=""
                               aria-describedby="sample-table-2_info">

                            <thead>

                            <tr>

                                <th style="width: 60px;">Username</th>

                                <th style="width: 60px;">Email</th>

                                <th style="width: 100px;">Mobile Number</th>

                                <th style="width: 100px;">Home Number</th>

                                <th style="width: 120px;">Address</th>

                                <th style="width: 100px;">Created At</th>

                                <th style="width: 100px;">Updated At</th>

                                <th style="width: 50px;">Status</th>

                            </tr>

                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <tr class="odd">
                                <td>{{ $data['row']->username }} </td>
                                <td>
                                    @if($data['row']->email)
                                        {{ $data['row']->email }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>{{ $data['row']->mobile_number }} </td>
                                <td>
                                    @if($data['row']->home_number)
                                        {{ $data['row']->home_number }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>
                                    @if($data['row']->address)
                                        {{ $data['row']->address }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>{{ date('jS M, Y', strtotime($data['row']->created_at)) }} </td>
                                <td>{{ date('jS M, Y', strtotime($data['row']->updated_at)) }} </td>
                                <td>
                                    @if($data['row']->status == 1)
                                        <button class="btn btn-minier btn-primary">Active</button>
                                    @else
                                        <button class="btn btn-minier btn-yellow">Inactive</button>
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="page-header">

                        <h1>

                            <small>
                                <i class="icon-double-angle-right"></i>
                                Cases Of Customer : {{ $data['row']->fullname }}
                            </small>

                        </h1>

                    </div>

                    <div class="col-xs-12">

                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">

                            <thead>

                                <tr>
                                    <th style="width: 20px;">S.N.</th>

                                    <th style="width: 120px;">Case Name</th>

                                    <th style="width: 80px;">Item Brand</th>

                                    <th style="width: 80px;">Item Model</th>

                                    <th style="width: 150px;">Item Specs</th>

                                    <th style="width: 50px;">Charge</th>

                                    <th style="width: 50px;">Entry Date</th>

                                    <th style="width: 50px;">Case History</th>

                                </tr>

                            </thead>

                            <tbody>
                            @if($data['cases'] != null)

                                <?php $i = 0 ?>
                                @foreach($data['cases'] as $row)
                                    <?php $i ++ ?>

                                    <tr>
                                        <td>{{ $i }}</td>

                                        <td>{{ $row->case_name }}</td>

                                        <td>{{ $row->item_brand }}</td>

                                        <td>{{ $row->item_model }}</td>

                                        <td>{{ $row->item_specs }}</td>

                                        <td>Rs. {{ $row->minimum_charge }}</td>

                                        <td>{{ $row->case_date }}</td>

                                        <td>
                                            <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

                                                <a class="blue"
                                                   href="{{ route('admin.cases.view', ['id' => $row->id]) }}">
                                                    <i class="icon-zoom-in bigger-130" title="View Case History"></i>
                                                </a>

                                            </div>

                                            <div class="visible-xs visible-sm hidden-md hidden-lg">

                                                <div class="inline position-relative">

                                                    <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        <i class="icon-caret-down icon-only bigger-120"></i>
                                                    </button>

                                                    <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">

                                                        <li>
                                                            <a href="#" class="tooltip-info" data-rel="tooltip"
                                                               title="View">
                                                                <span class="blue"><i
                                                                            class="icon-zoom-in bigger-120"></i></span>
                                                            </a>
                                                        </li>

                                                    </ul>

                                                </div>

                                            </div>

                                        </td>

                                    </tr>

                                @endforeach

                            </tbody>

                            @else
                                <tr>
                                    <td colspan="8">There is no cases for this customer.</td>
                                </tr>
                            @endif

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page_specific_scripts')

@endsection

