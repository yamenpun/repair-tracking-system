<!DOCTYPE html>

<html>

    <head>

        <title>Convert To PDF</title>

        <style>

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 10px;
            }

        </style>

    </head>

    <body>

        <div style="overflow-x:auto;">

            <h2 style="text-align: center">Vendor List</h2>

            <table border="1">

                <tr>

                    <th style="width: 10px;">S.N.</th>

                    <th style="width: 80px;">Vendor Name</th>

                    <th style="width: 70px;">Contact Person</th>

                    <th style="width: 40px;">Username</th>

                    <th style="width: 50px;">Email Address</th>

                    <th style="width: 50px;">Mobile Number</th>

                    <th style="width: 50px;">Home Number</th>

                    <th style="width: 50px;">Address</th>

                </tr>

                <?php $i = 0 ?>

                @foreach($data['rows'] as $row)

                    <?php $i ++ ?>

                    <tr>

                        <td>{{ $i }}</td>

                        <td>{{ $row->office_name }}</td>

                        <td>{{ $row->fullname }}</td>

                        <td>{{ $row->username }}</td>

                        <td>
                            @if($row->email)
                                {{ $row->email }}
                            @else
                                N/A
                            @endif
                        </td>

                        <td>{{ $row->mobile_number }}</td>

                        <td>
                            @if($row->home_number)
                                {{ $row->home_number }}
                            @else
                                N/A
                            @endif
                        </td>

                        <td>
                            @if($row->address)
                                {{ $row->address }}
                            @else
                                N/A
                            @endif
                        </td>

                    </tr>

                @endforeach

            </table>

        </div>

    </body>

</html>
