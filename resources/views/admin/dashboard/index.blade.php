@extends('admin.layouts.master')

@section('page_specific_style')

    <style>
        .acb {
            display: block;
        }
    </style>

@endsection

@section('title')
    Dashboard
@endsection

@section('content')


    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li class="active">Dashboard</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Welcome To Dashboard Manager
                    </small>

                </h1>

            </div>


            <div class="col-xs-12">

                <div class="alert alert-block alert-success">

                    <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button>

                    <i class="icon-ok green"></i>

                    Hi, <b>{{ ucfirst(Auth::user()->username) }}</b> welcome to the dashboard of REPAIR TRACKING SYSTEM.

                </div>

            </div>

            <div class="col-xs-12">

                <div class="col-xs-9 infobox-container">

                    <div class="infobox infobox-blue">
                        <div class="infobox-icon">
                            <i class="icon-plus"></i>
                        </div>

                        <div class="infobox-data">
                            <div class="infobox-content"><a href="{{ route('admin.customer.add') }}"><h5>ADD CUSTOMER</h5></a></div>
                        </div>
                    </div>

                    <div class="infobox infobox-blue">
                        <div class="infobox-icon">
                            <i class="icon-plus"></i>
                        </div>

                        <div class="infobox-data">
                            <div class="infobox-content"><a href="{{ route('admin.cases.add') }}"><h5>ADD CASE</h5></a></div>
                        </div>
                    </div>

                    <div class="infobox infobox-blue">
                        <div class="infobox-icon">
                            <i class="icon-plus"></i>
                        </div>

                        <div class="infobox-data">
                            <div class="infobox-content"><a href="{{ route('admin.items.add') }}"><h5>ADD ITEM</h5></a></div>
                        </div>
                    </div>

                    <div class="space-6"></div>

                    <div class="infobox infobox-green">
                        <div class="infobox-icon">
                            <i class="icon-group"></i>
                        </div>

                        <div class="infobox-data">
                            <span class="infobox-data-number">{{ $data['no_of_customers'] }}</span>
                            <div class="infobox-content"><a href="{{ route('admin.customer') }}"><h5>Customers</h5></a></div>
                        </div>
                    </div>

                    <div class="infobox infobox-green">
                        <div class="infobox-icon">
                            <i class="icon-cogs"></i>
                        </div>

                        <div class="infobox-data">
                            <span class="infobox-data-number">{{ $data['no_of_cases'] }}</span>
                            <div class="infobox-content"><a href="{{ route('admin.cases.list') }}"><h5>Cases</h5></a></div>
                        </div>
                    </div>

                    <div class="infobox infobox-green">
                        <div class="infobox-icon">
                            <i class="icon-credit-card"></i>
                        </div>

                        <div class="infobox-data">
                            <span class="infobox-data-number">{{ $data['no_of_case_history'] }}</span>
                            <div class="infobox-content"><a href="#"><h5>Cases History</h5></a></div>
                        </div>
                    </div>

                    <div class="space-6"></div>

                    {{--<div class="infobox infobox-green infobox-large infobox-dark">
                        <div class="infobox-data">
                            <div class="infobox-content">Total Transaction Amount</div>
                            <div class="infobox-content">
                                444,444
                            </div>
                        </div>
                    </div>

                    <div class="infobox infobox-orange infobox-large infobox-dark">
                        <div class="infobox-data">
                            <div class="infobox-content">Total Paid Amount</div>
                            <div class="infobox-content">
                                222,222
                            </div>
                        </div>
                    </div>

                    <div class="infobox infobox-red infobox-large infobox-dark">
                        <div class="infobox-data">
                            <div class="infobox-content">Total Due Amount</div>
                            <div class="infobox-content">
                                12, 222
                            </div>
                        </div>
                    </div>--}}

                </div>

            </div>

        </div>

    </div>

@endsection

