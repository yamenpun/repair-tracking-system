<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('admin/dashboard')?'class="active"':'' !!}>

            <a href="{{ route('admin.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

        </li>

        <li {!! Request::is('admin/customer*')?'class="active"':'' !!}>

            <a href="{{ route('admin.customer') }}">
                <i class="icon-group"></i>
                <span class="menu-text"> Customer </span>
            </a>

        </li>

        <li {!! Request::is('admin/cases/list*') || Request::is('admin/cases/view*')
              || Request::is('admin/cases/add*') || Request::is('admin/cases/edit*')
              || Request::is('admin/cases/comment*') || Request::is('admin/case/comment/history*')
              || Request::is('admin/cases/history/add*') || Request::is('admin/cases/history/edit*')
              || Request::is('admin/cases/history/view*') || Request::is('admin/cases/transfer/*') ?'class="active"':'' !!}>

            <a href="{{ route('admin.cases.list') }}">
                <i class="icon-gears"></i>
                <span class="menu-text"> Cases </span>
            </a>

        </li>

        <li {!! Request::is('admin/company/list*') || Request::is('admin/company/view*') || Request::is('admin/company/add*') || Request::is('admin/company/edit*') ?'class="active"':'' !!}>

            <a href="{{ route('admin.company.list') }}">
                <i class="icon-building"></i>
                <span class="menu-text"> Vendor </span>
            </a>

        </li>

        <li {!! Request::is('admin/items/list*') || Request::is('admin/items/view*') || Request::is('admin/items/add*') || Request::is('admin/items/edit*') ?'class="active"':'' !!}>

            <a href="{{ route('admin.items.list') }}">
                <i class="icon-plus-sign"></i>
                <span class="menu-text"> Items </span>
            </a>

        </li>

        <li {!! Request::is('admin/user*')?'class="active"':'' !!}>

            <a href="{{ route('admin.user') }}">
                <i class="icon-user"></i>
                <span class="menu-text"> User </span>
            </a>

        </li>

        <li>

            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>

        </li>

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">

        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }

    </script>

</div>

