<div class="navbar navbar-default" id="navbar">

    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
    </script>

    <div class="navbar-container" id="navbar-container">

        <div class="navbar-header pull-left">

            <a href="{{ route('admin.dashboard') }}" class="navbar-brand">

                <small>
                    <i class="icon-leaf"></i>
                    REPAIR TRACKING SYSTEM
                </small>

            </a>

        </div>

        <div class="navbar-header pull-right" role="navigation">

            <ul class="nav ace-nav">

                {{--<li class="purple">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon-bell-alt icon-animated-bell"></i>
                        <span class="badge badge-important">--}}{{--{{ $notifications_count['0']->notifications }}--}}{{--</span>
                    </a>

                    <ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
                        <li class="dropdown-header">
                            <i class="icon-warning-sign"></i>
                            --}}{{--{{ $notifications_count['0']->notifications }} Customer Comments--}}{{--
                        </li>
                       --}}{{-- @foreach($notifications_details as $data)
                            <li>
                                <a href="{{ route('admin.cases.comment.list', ['id' => $data->id]) }}">
                                    <div class="clearfix">
                                        <span class="pull-left">
                                            <i class="btn btn-xs no-hover btn-pink icon-comment"></i>
                                            {{ $data->fullname }}
                                        </span>
                                        <span class="pull-right badge badge-info">+{{ $data->no_of_comments }}</span>
                                    </div>
                                </a>
                            </li>
                        @endforeach--}}{{--
                    </ul>
                </li>
--}}
                <li class="light-blue">

                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">

                        <img class="nav-user-photo" src="{{ asset('assets/admin/avatars/user.png') }}" alt=""/>

                            <span class="user-info">

                                <small>Welcome,</small>

                                {{ Auth::user()->username }}

                            </span>

                        <i class="icon-caret-down"></i>

                    </a>

                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                        <li>

                            <a href="{{ route('admin.user.profile') }}">
                                <i class="icon-user"></i>
                                User Profile
                            </a>

                        </li>

                        <li class="divider"></li>

                        <?php $user = Auth::user() ?>

                        <li>

                            <a href="{{ route('admin.user.edit.password', ['id' => $user->id]) }}">
                                <i class="icon-edit"></i>
                                Change Password
                            </a>

                        </li>

                        <li class="divider"></li>

                        <li>

                            <a href="{{ url('logout') }}">
                                <i class="icon-off"></i>
                                Logout
                            </a>

                        </li>

                    </ul>

                </li>

            </ul>

        </div>

    </div>

</div>