<!DOCTYPE html>

<html>

    <head>

        <title>Convert To PDF</title>

        <style>

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 12px;
            }

        </style>

    </head>

    <body>

        <div style="overflow-x:auto;">

            <h2 style="text-align: center">Items List</h2>

            <table border="1">

                <tr>
                    <th style="width: 10px;">S.N.</th>

                    <th style="width: 100px;">Item Type</th>

                    <th style="width: 200px;">Items Description</th>

                    <th style="width: 80px;">Created At</th>

                    <th style="width: 80px;">Updated At</th>

                </tr>

                <?php $i = 0 ?>

                @foreach($data['rows'] as $row)

                    <?php $i++ ?>

                    <tr>

                        <td>{{ $i }}</td>

                        <td>{{ $row->item_type }}</td>

                        <td>{{ $row->item_description }}</td>

                        <td>{{ $row->created_at }}</td>

                        <td>{{ $row->updated_at }}</td>

                    </tr>

                @endforeach

            </table>

        </div>

    </body>

</html>
