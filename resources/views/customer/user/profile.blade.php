@extends('customer.layouts.master')

@section('title')
    User Profile
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('customer.dashboard') }}">Home</a>
                </li>

                <li class="active">User Profile</li>

            </ul><!-- .breadcrumb -->

        </div>

        <div class="page-content">

            <div class="page-header">
                <h1>
                    <small>
                        <i class="icon-double-angle-right"></i>
                        User Profile
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm ">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>
            </div><!-- /.page-header -->

            <div class="row">

                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div>

                        <div id="user-profile-1" class="user-profile row">

                            <div class="col-xs-12 col-sm-9">

                                <div class="profile-user-info profile-user-info-striped">

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Fullname</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="username">{{ $data['rows']->fullname }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Username</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="username">{{ $data['rows']->username }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Email</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="email">{{ $data['rows']->email }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Office Name</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="email">@if($data['rows']->office_name){{ $data['rows']->office_name }}@else N/A @endif</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Address</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="email">@if($data['rows']->address){{ $data['rows']->address }}@else N/A @endif</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Mobile No.</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="role">{{ $data['rows']->mobile_number }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name">Telephone No.</div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="role">@if($data['rows']->home_number){{ $data['rows']->home_number }}@else N/A @endif</span>
                                        </div>

                                    </div>

                                </div>

                                <div class="space-6"></div>

                            </div>

                        </div>

                    </div>

                    <!-- PAGE CONTENT ENDS -->

                </div>

            </div>

        </div>

    </div>

@endsection