<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('customer/dashboard')?'class="active"':'' !!}>

            <a href="{{ route('customer.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

        </li>

        <li {!! Request::is('customer/account') || Request::is('customer/viewCaseHistory*') || Request::is('customer/viewCaseComment*') ||
                Request::is('customer/user/profile') || Request::is('customer/user/edit*') || Request::is('customer/case/history/view*') ||
                Request::is('customer/case/comment/view*') || Request::is('customer/case/history/comment/view*') ?'class="active"':'' !!}>

            <a href="{{ route('customer.account') }}">
                <i class="icon-user"></i>
                <span class="menu-text"> My Account </span>
            </a>

        </li>

        <li>

            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>

        </li>

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">

        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }

    </script>

</div>

