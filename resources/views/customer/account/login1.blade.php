@extends('customer.layouts.customer_master')

@section('title')
    Customer Login
@endsection

@section('content')
    <div class="position-relative center">
        <div id="login-box" class="login-box visible widget-box">
            <div class="widget-body">
                <div class="widget-main">
                    <h4 class="header blue lighter bigger">
                        <i class="icon-coffee green"></i>
                        Please Enter Your Case Code
                    </h4>

                    <div class="space-6"></div>

                    <form method="POST" action="{{ url('/customer/login') }}">

                        {!! csrf_field() !!}

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <fieldset>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="case_code" value="{{ old('case_code') }}" placeholder="Enter Case Code"/>
                                    <i class="icon-user"></i>
                                </span>
                            </label>

                            <div class="space"></div>

                            <div class="clearfix">
                                <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                    <i class="icon-key"></i>
                                    Submit
                                </button>
                            </div>

                            <div class="space-4"></div>
                        </fieldset>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection




