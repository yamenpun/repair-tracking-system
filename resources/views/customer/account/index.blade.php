@extends('customer.layouts.master')

@section('title')
    My Account
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('customer.dashboard') }}">Home</a>
                </li>

                <li class="active">My Account</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        My Account Info
                    </small>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="table-responsive">

                        @if (session()->has('message'))
                            {!! session()->get('message') !!}
                        @endif

                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">

                            <thead>

                            <tr>
                                <th style="width: 80px;">Full Name</th>

                                <th style="width: 60px;">Username</th>

                                <th style="width: 50px;">Email</th>

                                <th style="width: 100px;">Office Name</th>

                                <th style="width: 100px;">Contact Number</th>

                                <th style="width: 100px;">Current Address</th>

                            </tr>

                            </thead>

                            <tbody>
                                <tr>
                                    <td>{{ $data['row']['0']->fullname }}</td>

                                    <td>{{ $data['row']['0']->username }}</td>

                                    <td>
                                        @if($data['row']['0']->email)
                                            {{ $data['row']['0']->email }}
                                        @else
                                            N/A
                                        @endif
                                    </td>

                                    <td>
                                        @if($data['row']['0']->office_name)
                                            {{ $data['row']['0']->office_name }}
                                        @else
                                            N/A
                                        @endif
                                    </td>

                                    <td>
                                        {{ $data['row']['0']->mobile_number }}
                                        @if($data['row']['0']->home_number)
                                            , {{ $data['row']['0']->home_number }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($data['row']['0']->address)
                                            {{ $data['row']['0']->address }}
                                        @else
                                            N/A
                                        @endif
                                    </td>

                                </tr>

                            </tbody>

                        </table>

                    </div>

                </div>

                <div class="col-xs-12">

                    <div class="page-header">

                        <h1>

                            <small>
                                <i class="icon-double-angle-right"></i>
                                My Cases Details
                            </small>

                        </h1>

                    </div>

                    <table id="sample-table-2" class="table table-striped table-bordered table-hover">

                        <thead>

                        <tr>
                            <th style="width: 20px;">S.N.</th>

                            <th style="width: 50px;">Entry Date</th>

                            <th style="width: 50px;">Case Code</th>

                            <th style="width: 120px;">Case Name</th>

                            <th style="width: 150px;">Item Details</th>

                            <th style="width: 50px;">Charge</th>

                            <th style="width: 80px;">Action</th>

                        </tr>

                        </thead>

                        <tbody>
                        @if($data['cases'] != null)

                            <?php $i = 0 ?>
                            @foreach($data['cases'] as $row)
                                <?php $i ++ ?>

                                <tr>
                                    <td>{{ $i }}</td>

                                    <td>{{ $row->case_date }}</td>

                                    <td>{{ $row->case_code }}</td>

                                    <td>{{ $row->case_name }}</td>

                                    <td>
                                        @if($row->item_brand)
                                            {{ $row->item_brand }}
                                        @endif
                                        @if($row->item_model)
                                            , {{ $row->item_model }}
                                        @endif
                                        @if($row->item_specs)
                                            , {{ $row->item_specs }}
                                        @endif
                                    </td>

                                    <td>
                                        @if($row->minimum_charge == '0')
                                            N/A
                                        @else
                                            Rs. {{ $row->minimum_charge }}
                                        @endif
                                    </td>

                                    <td>
                                        <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

                                            <a class="blue"
                                               href="{{ route('customer.case.history.view', ['id' => $row->id]) }}">
                                                <i class="icon-zoom-in bigger-130" title="View Case History"></i>
                                            </a>

                                            <a class="blue"
                                               href="{{ route('customer.case.comment.view', ['id' => $row->id]) }}">
                                                <i class="icon-comment-alt bigger-130" title="View Case Comments"></i>
                                            </a>

                                        </div>

                                        <div class="visible-xs visible-sm hidden-md hidden-lg">

                                            <div class="inline position-relative">

                                                <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    <i class="icon-caret-down icon-only bigger-120"></i>
                                                </button>

                                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">

                                                    <li>
                                                        <a href="#" class="tooltip-info" data-rel="tooltip"
                                                           title="View">
                                                                <span class="blue"><i
                                                                            class="icon-zoom-in bigger-120"></i></span>
                                                        </a>
                                                    </li>

                                                </ul>

                                            </div>

                                        </div>

                                    </td>

                                </tr>

                            @endforeach

                        </tbody>

                        @else
                            <tr>
                                <td colspan="8">There is no cases for this customer.</td>
                            </tr>
                        @endif

                    </table>
                </div>

            </div>


        </div>

    </div>

@endsection
