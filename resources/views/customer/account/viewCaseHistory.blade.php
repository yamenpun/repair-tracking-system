@extends('customer.layouts.master')

@section('title')
    View Case History
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('customer.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route('customer.account') }}">My Account</a>
                </li>

                <li class="active">View Case History</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Case History Of Case : {{ $data['case']['0']->case_name }}
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <table class="table table-striped table-bordered table-hover "
                           id="" aria-describedby="sample-table-2_info">

                        <thead>

                            <tr>
                                <th style="width: 20px;">S.N.</th>

                                <th style="width: 50px;">Entry Date</th>

                                <th style="width: 50px;">Item Status</th>

                                <th style="width: 120px;">Description</th>

                                <th style="width: 70px;">Estimated Time</th>

                                <th style="width: 50px;">Charge</th>

                                <th style="width: 80px;">Remarks</th>

                                <th style="width: 80px;">Action</th>

                            </tr>

                        </thead>

                        <tbody>
                        @if($data['case_history'] != null)

                            <?php $i = 0 ?>
                            @foreach($data['case_history'] as $row)
                                <?php $i ++ ?>

                                <tr>
                                    <td>{{ $i }}</td>

                                    <td>{{ $row->case_history_date }}</td>

                                    <td>{{ $row->item_status }}</td>

                                    <td>{{ $row->description }}</td>

                                    <td>
                                        @if($row->etr)
                                            {{ $row->etr }}
                                        @else
                                            N/A
                                        @endif
                                    </td>

                                    <td>
                                        @if($row->total_charge != '0')
                                            Rs. {{ $row->total_charge }}
                                        @else
                                            N/A
                                        @endif
                                    </td>

                                    <td>
                                        @if($row->remarks)
                                            {{ $row->minimum_charge }}
                                        @else
                                            N/A
                                        @endif
                                    </td>

                                    <td>
                                        <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

                                            <a class="blue"
                                               href="{{ route('customer.case.history.comment.view', ['id' => $row->id]) }}">
                                                <i class="icon-comment-alt bigger-130" title="View Case History Comment"></i>
                                            </a>

                                        </div>

                                        <div class="visible-xs visible-sm hidden-md hidden-lg">

                                            <div class="inline position-relative">

                                                <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    <i class="icon-caret-down icon-only bigger-120"></i>
                                                </button>

                                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">

                                                    <li>
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" title="View">
                                                            <span class="blue">
                                                                <i class="icon-zoom-in bigger-120"></i>
                                                            </span>
                                                        </a>
                                                    </li>

                                                </ul>

                                            </div>

                                        </div>
                                    </td>

                                </tr>

                            @endforeach

                        @else
                            <tr>
                                <td colspan="8">No history recorded for this case.</td>
                            </tr>
                        @endif

                        </tbody>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

@endsection
