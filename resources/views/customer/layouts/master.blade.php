@if(Auth::user()->role === 'customer')

    <!DOCTYPE html>

    <html lang="en">

        <head>

            <title> RTS :: @yield('title') </title>

            <meta charset="utf-8"/>

            <meta name="description" content="overview &amp; stats"/>

            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

            <!-- basic styles -->

            <link href="{{ asset('assets/admin/css/bootstrap.min.css') }}" rel="stylesheet"/>

            <link rel="stylesheet" href="{{ asset('assets/admin/css/font-awesome.min.css') }}"/>

            <!--[if IE 7]>
            <link rel="stylesheet" href="{{ asset('assets/admin/css/font-awesome-ie7.min.css') }}"/>

            <![endif]-->

            <!-- fonts -->

            <link rel="stylesheet" href="{{ asset('assets/admin/css/ace-fonts.css') }}"/>

            <!-- ace styles -->

            <link rel="stylesheet" href="{{ asset('assets/admin/css/ace.min.css') }}"/>

            <link rel="stylesheet" href="{{ asset('assets/admin/css/ace-rtl.min.css') }}"/>

            <link rel="stylesheet" href="{{ asset('assets/admin/css/ace-skins.min.css') }}"/>

            <!--[if lte IE 8]>
            <link rel="stylesheet" href="{{ asset('assets/admin/css/ace-ie.min.css') }}"/>
            <![endif]-->


            @yield('page_specific_style')

            <!-- ace settings handler -->

            <script src="{{ asset('assets/admin/js/ace-extra.min.js') }}"></script>

            <!--[if lt IE 9]>

            <script src="{{ asset('assets/admin/js/html5shiv.js') }}"></script>

            <script src="{{ asset('assets/admin/js/respond.min.js') }}"></script>

            <![endif]-->
            <script type="text/javascript">
                window.jQuery || document.write("<script src='{{ asset('assets/admin/js/jquery-2.0.3.min.js') }}'>" + "<" + "/script>");
            </script>

            <!-- <![endif]-->

            <!--[if IE]>

            <script type="text/javascript">
                window.jQuery || document.write("<script src='{{ asset('assets/admin/js/jquery-1.10.2.min.js') }}'>" + "<" + "/script>");
            </script>

            <![endif]-->

            <script type="text/javascript">
                if ("ontouchend" in document) document.write("<script src='{{ asset('assets/admin/js/jquery.mobile.custom.min.js') }}}'>" + "<" + "/script>");
            </script>

            <script src="{{ asset('assets/admin/js/bootstrap.min.js') }}"></script>

            <script src="{{ asset('assets/admin/js/typeahead-bs2.min.js') }}"></script>

            <script src="https://www.gstatic.com/firebasejs/live/3.0/firebase.js"></script>

            <style>
                .validation-error {
                    color: red;
                }
            </style>

        </head>


        <body>

            @include('customer.includes.header')

            <div class="main-container" id="main-container">

                <script type="text/javascript">
                    try {
                        ace.settings.check('main-container', 'fixed')
                    } catch (e) {
                    }
                </script>

                <div class="main-container-inner">

                    <a class="menu-toggler" id="menu-toggler" href="#">
                        <span class="menu-text"></span>
                    </a>

                    @include('customer.includes.left-sidebar')

                    @yield('content')

                </div>

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>

            </div>

            <!--[if lte IE 8]>
            <script src="{{ asset('assets/admin/js/excanvas.min.js') }}"></script>
            <![endif]-->

            <script src="{{ asset('assets/admin/js/jquery-ui-1.10.3.custom.min.js') }}"></script>

            <script src="{{ asset('assets/admin/js/jquery.ui.touch-punch.min.js') }}"></script>

            <script src="{{ asset('assets/admin/js/jquery.slimscroll.min.js') }}"></script>

            <script src="{{ asset('assets/admin/js/bootbox.min.js') }}"></script>

            <!-- ace scripts -->

            <script src="{{ asset('assets/admin/js/ace-elements.min.js') }}"></script>

            <script src="{{ asset('assets/admin/js/ace.min.js') }}"></script>

            <script src="{{ asset('assets/admin/js/require.js') }}"></script>

            <script src="{{ asset('assets/admin/js/jquery.validate.min.js') }}"></script>

            <!-- inline scripts related to this page -->

            @yield('page_specific_scripts')

            <script>

                $(document).ready(function () {

                    $(".disableConfirm").on(ace.click_event, function(e) {
                        e.preventDefault();
                        attr = this;
                        bootbox.confirm("Are you sure to disable this record?", function(result) {
                            if(result) {
                                location.href = attr.href;
                            }
                        });
                    });

                    $(".deleteConfirm").on(ace.click_event, function(e) {
                        e.preventDefault();
                        attr = this;
                        bootbox.confirm("Are you sure to permanently delete this record from database?", function(result) {
                            if(result) {
                                location.href = attr.href;
                            }
                        });
                    });

                    jQuery(function($) {
                        $('.date-picker').datepicker({format: 'yyyy-mm-dd',autoclose:true}).next().on(ace.click_event, function(){
                            $(this).prev().focus();
                        });
                    });

                });

            </script>

        </body>

    </html>
@else
    @include('errors.401')
@endif
