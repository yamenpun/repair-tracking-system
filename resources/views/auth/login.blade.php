@extends('layouts.app')

@section('title')
    Login
@endsection

@section('content')

    <div class="position-relative center">
        <div id="login-box" class="login-box visible widget-box">
            <div class="widget-body">
                <div class="widget-main">
                    <h3 class="blue">&copy; Repair Tracking System</h3>
                    <h5 class="header blue lighter bigger">
                        <i class="icon-coffee green"></i>
                        Please Enter Your Information
                    </h5>

                    <div class="space-6"></div>

                    <form method="POST" action="{{ url('/login') }}">

                        {!! csrf_field() !!}

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <fieldset>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username"/>
                                    <i class="icon-user"></i>
                                </span>
                            </label>

                            <div class="space"></div>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="password" class="form-control" name="password" placeholder="Password"/>
                                    <i class="icon-lock"></i>
                                </span>
                            </label>

                            <div class="space"></div>

                            <div class="clearfix">
                                <label class="inline">
                                    <input type="checkbox" name="remember" class="ace"/>
                                    <span class="lbl"> Remember Me</span>
                                </label>

                                <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                    <i class="icon-key"></i>
                                    Login
                                </button>
                            </div>

                            <div class="space-4"></div>
                        </fieldset>
                    </form>

                </div>

            </div>
        </div>
    </div>

@endsection