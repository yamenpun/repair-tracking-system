<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>RTS :: @yield('title')</title>

        <meta name="description" content="User login page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- script  for Ck editor -->
        <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
        <script>
            CKEDITOR.replace( 'article-ckeditor' );
        </script>

        <!-- basic styles -->

        <link href="{{ asset('assets/admin/css/bootstrap.min.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('assets/admin/css/font-awesome.min.css') }}" />

        <link rel="stylesheet" href="{{ asset('assets/admin/css/ace-fonts.css') }}" />

        <!-- ace styles -->

        <link rel="stylesheet" href="{{ asset('assets/admin/css/ace.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/admin/css/ace-rtl.min.css') }}" />
    </head>

    <body class="login-layout">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <div style="margin: 0 auto;">
                                    <img width="380px" height="100px" src="{{ asset('assets/images/logo/laptopnepal.png') }}" />
                                </div>
                            </div>

                            @yield('content')

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            window.jQuery || document.write("<script src='{{ asset('assets/admin/js/jquery-2.0.3.min.js') }}'>"+"<"+"/script>");
        </script>

        <script type="text/javascript">
            if("ontouchend" in document) document.write("<script src='{{ asset('assets/admin/js/jquery.mobile.custom.min.js') }}}'>"+"<"+"/script>");
        </script>

        <!-- inline scripts related to this page -->

        <script type="text/javascript">
            function show_box(id) {
                jQuery('.widget-box.visible').removeClass('visible');
                jQuery('#'+id).addClass('visible');
            }
        </script>
    </body>
</html>
