@extends('normal.layouts.login')

@section('title')
    Code Login
@endsection

@section('content')

    <div style="height: auto; width: 400px; margin: 0 550px 0 450px;">

        <div style="padding-top: 20px;">
            <nav>
                <ul class="nav nav-pills pull-right">
                    <li><a href="{{ route('admin.login') }}" style="font-size: 20px; color: red;">LOGIN</a></li>
                </ul>
            </nav>
            <div class="jumbotron">

                <h2 class="blue center">&copy; Repair Tracking System</h2>
                <h5 class="header blue lighter bigger center">
                    <i class="icon-coffee green"></i>
                    Please Enter Your Information
                </h5>
                <form method="POST" action="{{ url('/normal/login') }}">

                    {!! csrf_field() !!}

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    @endif

                    <fieldset>
                        <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <input type="text" class="form-control" name="case_code" value="{{ old('case_code') }}" placeholder="Enter Case Code"/>
                                <i class="icon-code"></i>
                            </span>
                        </label>

                        <div class="space"></div>

                        <div class="clearfix">
                            <button type="submit" class="width-25 pull-right btn btn-sm btn-primary">
                                <i class="icon-forward"></i>
                                Next
                            </button>
                        </div>
                    </fieldset>
                </form>
                </div>
            </div>
        </div>
    </div>

@endsection




