@if (Session::has('session_code'))

    @extends('normal.layouts.login')

    @section('title')
        Name Login
    @endsection

    @section('content')
        <div style="height: auto; width: 400px; margin: 0 auto;">

            <div style="padding-top: 20px;">

                <div class="jumbotron">

                    <h4 class="green lighter bigger ">
                        <i class="icon-coffee green"></i>
                        Please enter your full name
                    </h4>
                    <form method="POST" action="{{ url('/normal/name/login') }}">

                        {!! csrf_field() !!}

                        @if (session()->has('message'))
                            {!! session()->get('message') !!}
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                        {{ $error }}
                                @endforeach
                            </div>
                        @endif

                        <fieldset>
                            <label class="block clearfix">
                                    <span class="block input-icon input-icon-right">
                                        <input type="text" class="form-control" name="fullname"
                                               value="{{ old('fullname') }}" placeholder="Enter Full Name"/>
                                        <i class="icon-user"></i>
                                    </span>
                            </label>

                            <div class="space"></div>

                            <div class="clearfix">
                                <button type="submit" class="width-25 pull-right btn btn-sm btn-primary">
                                    <i class="icon-signin"></i>
                                    Login
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

    @endsection
@else
    @include('errors.401');
@endif





