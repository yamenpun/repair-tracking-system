@extends('normal.layouts.master')

@section('title')
    Normal Dashboard
@endsection

@section('content')

    <div class="page-content">

        <div class="page-header">

            <h1>

                <small>
                    <i class="icon-double-angle-right"></i>
                    Case Detail Info
                </small>
            </h1>

        </div>

        <div class="page-content">

            <div class="row">

                <div class="col-xs-12">

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                    <h4 class="row smaller lighter blue">
                        <span class="col-xs-6">Case Name: <b>{{ $data['row']['0']->case_name }}</b></span>
                    </h4>

                    <table class="table table-striped table-bordered table-hover" id=""
                           aria-describedby="sample-table-2_info">

                        <thead>

                        <tr>

                            <th style="width: 60px;">Entry Date</th>

                            <th style="width: 60px;">Case Code</th>

                            <th style="width: 80px;">Item Name</th>

                            <th style="width: 150px;">Item Details</th>

                            <th style="width: 60px;">Charge</th>

                            <th style="width: 60px;">Comment</th>

                        </tr>

                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">

                        <tr class="odd">
                            <td>{{ $data['row']['0']->case_date }} </td>
                            <td>{{ $data['row']['0']->case_code }} </td>
                            <td>{{ $data['row']['0']->item_name }} </td>
                            <td>{{ $data['row']['0']->item_brand }}
                                @if($data['row']['0']->item_model)
                                    , {{ $data['row']['0']->item_model }}
                                @endif
                                @if($data['row']['0']->item_specs)
                                    , {{ $data['row']['0']->item_specs }}
                                @endif
                            </td>
                            <td>
                                @if($data['row']['0']->minimum_charge == '0')
                                    N/A
                                @else
                                    Rs. {{ $data['row']['0']->minimum_charge }}
                                @endif
                            </td>
                            <td>
                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

                                    <a class="blue"
                                       href="{{ route('normal.case.comment', ['id' => $data['row']['0']->id]) }}">
                                        <i class="icon-comment-alt bigger-130" title="View Comment"></i>
                                    </a>

                                </div>

                                <div class="visible-xs visible-sm hidden-md hidden-lg">

                                    <div class="inline position-relative">

                                        <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                data-toggle="dropdown">
                                            <i class="icon-caret-down icon-only bigger-120"></i>
                                        </button>

                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">

                                            <li>
                                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Comment">
                                                    <span class="red"><i class="icon-trash bigger-120"></i></span>
                                                </a>
                                            </li>

                                        </ul>

                                    </div>

                                </div>
                            </td>

                        </tr>

                        </tbody>

                    </table>

                    <h4 class="row smaller lighter blue">
                        <span class="col-xs-6">Case History Details </span>
                    </h4>

                    <table class="table table-striped table-bordered table-hover" id=""
                           aria-describedby="sample-table-2_info">

                        <thead>

                        <tr>

                            <th style="width: 60px;">Entry Date</th>

                            <th style="width: 60px;">Item Status</th>

                            <th style="width: 150px;">Description</th>

                            <th style="width: 60px;">Image</th>

                            <th style="width: 60px;">Estimated Time</th>

                            <th style="width: 60px;">Total Charge</th>

                            <th style="width: 60px;">Remarks</th>

                            <th style="width: 60px;">Comment</th>

                        </tr>

                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">

                        @if($data['case_history'] != null)

                            @foreach($data['case_history'] as $case_history)

                                <tr class="odd">
                                    <td>{{ $case_history->case_history_date }} </td>
                                    <td>{{ $case_history->item_status }} </td>
                                    <td>{{ $case_history->description }} </td>
                                    <td>
                                        @if($case_history->image_one)
                                            <img src="{{ asset(config('global.url.frontend.image').'item/'.$case_history->image_one) }}"
                                                 alt=""
                                                 style="max-width: 200px; max-height:50px">
                                        @else
                                            <p>No Image Uploaded.</p>
                                        @endif
                                    </td>
                                    <td>
                                        @if($case_history->etr)
                                            {{ $case_history->etr }}
                                        @else
                                            N/A
                                        @endif
                                    </td>
                                    <td>
                                        @if($case_history->total_charge == '0')
                                            N/A
                                        @else
                                            Rs. {{ $case_history->total_charge }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($case_history->remarks)
                                            {{ $case_history->remarks }}
                                        @else
                                            N/A
                                        @endif
                                    </td>
                                    <td>
                                        <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

                                            <a class="blue"
                                               href="{{ route('normal.case.history.comment', ['id' => $case_history->id]) }}">
                                                <i class="icon-comment-alt bigger-130"
                                                   title="View Comment"></i>
                                            </a>

                                        </div>

                                        <div class="visible-xs visible-sm hidden-md hidden-lg">

                                            <div class="inline position-relative">

                                                <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    <i class="icon-caret-down icon-only bigger-120"></i>
                                                </button>

                                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">

                                                    <li>
                                                        <a href="#" class="tooltip-error" data-rel="tooltip"
                                                           title="Comment">
                                                            <span class="red"><i
                                                                        class="icon-trash bigger-120"></i></span>
                                                        </a>
                                                    </li>

                                                </ul>

                                            </div>

                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">No History to show.</td>
                            </tr>
                        @endif

                        </tbody>

                    </table>

                </div>
            </div>

        </div>

    </div>

@endsection

